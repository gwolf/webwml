  <h2><a name="what">HVA er Debian?</a></h2>


  <p>
    <a href="$(HOME)">Debian-prosjektet</a> er en samling av individer
    som har gjort det som felles mål å utvikle et <a href="free">fritt
    tilgjengelig</a> operativsystem. Dette operativsystemet heter
    <strong>Debian</strong>.
  </p>

  <p>
    Et operativsystem er samlingen av grunnleggende programmer og
    verktøyer som får maskinen din til å virke.  I senteret av
    operativsystemet er kjernen.  Kjernen er det mest fundamentale
    programmet på maskinen; den sørger for husholdningen, og lar deg
    starte andre programmer.</p>

  <p>
    Debian-systemer bruker for tiden
    <a href="https://www.kernel.org/">Linux</a>- eller 
    <a href="https://www.freebsd.org/">FreeBSD</a>-kjernen. 
    Linux er en helt fri programvare started av
    <a href="https://en.wikipedia.org/wiki/Linus_Torvalds">Linus Torvalds</a>
    og støttet av tusenvis av programmerere fra hele verden. FreeBSD 
    er et operativsystem som inkluderer en kjerne og annen programvare.
  </p>

  <p>
    Imidlertid er arbeid i gang for å gjøre Debian tilgjengelig for
    andre kjerner, først og fremst for
    <a href="https://www.gnu.org/software/hurd/hurd.html">Hurd</a>.
    Hurd er en samling av tjenester som opererer på toppen av en
    mikrokjerne (f.eks. Mach) for å utføre forskjellige oppgaver.
    Hurd er fri programvare, produsert av
    <a href="https://www.gnu.org/">GNU-prosjektet.</a></p>

  <p>
    En stor del av de grunnleggende verktøyene som gjør ut
    operativsystemet kommer fra <a href="https://www.gnu.org/">GNU</a>,
    derav navnene GNU/Linux, GNU/kFreeBSD og GNU/Hurd. 
    Disse verktøyene er også fri programvare.</p>
  
  <p>
    Selvfølgelig, det folk vil ha er programmer.
    Programmer til å hjelpe dem gjøre det de vil ha gjort, fra å
    redigere dokumenter til å drive en bedrift til å spille spill til å
    skrive mer programvare. Debian kommer med over <packages_in_stable>
    <a href="$(DISTRIB)/packages">pakker</a> (ferdigbygde programmer pakket
    inn i et egnet format for lettvint installering og oppgradering på
    maskinen din), en pakkebehandler (APT) og andre verktøy som gjør det
    mulig å administrere tusenvis av pakker på tusenvis av datamaskiner
    like enkelt som å installere et enkelt program.
    Alt sammen <a href="free">fritt tilgjengelig</a>.</p>

  <p>
    Sammenlign dette systemet med et tårn.  På bunnen er kjernen.
    Oppå her er alle de grunnleggende verktøyene.  Neste trinn er all
    programvaren som du kjører på maskinen din.  På toppen av tårnet er
    Debian &mdash; som omsorgsfullt organiserer og tilpasser alt slik at
    det virker i sammen.</p>

