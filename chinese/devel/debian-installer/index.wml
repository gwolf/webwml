#use wml::debian::template title="Debian 安装程序" NOHEADER="true"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="27016443323dac06f67403e3c8fddd149558e1e2"

[[s/介质/[CN:介质:][HKTW:媒介:]/g]]

<h1>新闻</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">更早的新闻</a>
</p>

<h1>使用 Debian 安装程序进行安装</h1>
 

<p>
<if-stable-release release="buster">
<strong>要寻找官方的 <current_release_buster> 安装介质和信息</strong>\
，请参阅 \
<a href="$(HOME)/releases/buster/debian-installer">buster 页面</a>。
</if-stable-release>
<if-stable-release release="bullseye">
<strong>要寻找官方的 <current_release_bullseye> 安装介质和信息</strong>\
，请参阅 \
<a href="$(HOME)/releases/bullseye/debian-installer">bullseye 页面</a>。
</if-stable-release>
</p>

<div class="tip">
<p>
以下链接指向的映像是用于测试为下个 Debian 发布版本开发的 \
Debian 安装程序的，将会默认安装 Debian testing\
（<q><current_testing_name></q>）。
</p>
</div>

<if-stable-release release="stretch">
<p>

<strong>To install Debian testing</strong>, we recommend you use
the <strong><humanversion /></strong> release of the installer, after checking its
<a href="errata">errata</a>. The following images are available for
<humanversion />:
<!--
<strong>To install Debian testing</strong>, we recommend you use
the <strong>daily builds</strong> of the installer. The following images are available for
daily builds:
-->
</p>

<h2>Official release</h2>

<div class="line">
<div class="item col50">
<strong>netinst (generally 180-450 MB) CD images</strong>
<netinst-images />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<full-dvd-images />
</div>

</div>


<div class="line">
<div class="item col50">
<strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>other images (netboot, USB stick, etc.)</strong>
<other-images />
</div>
</div>

<p>
Or install the <b>current weekly snapshot of Debian testing</b>
which uses the same version of the installer as the last release:
</p>

<h2>Current weekly snapshots</h2>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<devel-full-dvd-images />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>
</div>


</if-stable-release>
<!--
<p>
If you prefer to use the latest and greatest, either to help us test a future
release of the installer or because of hardware problems or other issues,
try one of these <strong>daily built images</strong> which contain the latest
available version of installer components.
</p>
-->

<h2>当前的每日快照</h2>

<div class="line">
<div class="item col50">
<strong>网络安装（通常 150-280MB） <!-- and businesscard (generally 20-50 MB) --> CD 映像</strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>网络安装 <!-- and businesscard --> CD 映像（通过 <a href="$(HOME)/CD/jigdo-cd">jigdo</a>）</strong>
<devel-small-cd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>网络安装，多架构 CD 映像</strong>
<devel-multi-arch-cd />
</div>

<div class="item col50 lastcol">
<strong>其他映像（网络启动、[CN:U 盘:][HK:USB 手指:][TW:USB 随身碟:]等）</strong>
<devel-other-images />
</div>
</div>

<p>
如果您系统的任何硬件的驱动<strong>需要加载固件\
</strong>才能工作，您可以使用\
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/">\
常见固件软件包的 tar [CN:文件:][HKTW:档:]</a>中的一个。\
使用这些 tar [CN:文件:][HKTW:档:]的步骤，以及\
关于在安装过程中加载固件的通用信息，请阅读安装手册（详见下方“文档”一节）。
</p>

<p>
<strong>注意</strong>
</p>
<ul>
#	<li>Before you download the daily built images, we suggest you check for
#	<a href="https://wiki.debian.org/DebianInstaller/Today">known issues</a>.</li>
	<li>如果某架构的每日映像不能（可靠地）被构建，概览中可能（暂时）缺少该架构的每日映像。</li>
	<li>对于安装映像，校验[CN:文件:][HKTW:档:]（<tt>SHA512SUMS</tt>\
	和 <tt>SHA256SUMS</tt>）可以在映像的同一目录下找到。</li>
	<li>下载完整的 CD 和 DVD 映像，推荐使用 jigdo。</li>
	<li>完整的 DVD 集中只有有限数量的映像可作为 ISO 文件直接下载。\
	大多数用户不需要所有光盘上的所有可用软件，所以为了节省\
	下载服务器和镜像站的空间，完整的映像集只能通过 jigdo 下载。</li>
	<li>多架构<em>网络安装 CD</em> 映像支持 i386/amd64；\
    安装过程与从单架构的网络安装映像安装相似。</li>
</ul>

<p>
<strong>使用 Debian 安装程序之后</strong>，即使没有任何问题，也请发一份\
<a href="https://d-i.debian.org/manual/zh_CN.amd64/ch05s04.html#submit-bug">安装报告</a>给我们。
</p>

<h1>文档</h1>

<p>
<strong>如果您在安装前只想阅读一份文档</strong>，请阅读我们的\
<a href="https://d-i.debian.org/manual/zh_CN.amd64/apa.html">安装指南</a>\
，这是一份安装过程的简要介绍。其他有用的文档包括：
</p>

<ul>
<li>安装手册：\
#    <a href="$(HOME)/releases/stable/installmanual">当前发布版本</a>
#    &mdash;
    <a href="$(HOME)/releases/testing/installmanual">development version (testing)</a>
    &mdash;
    <a href="https://d-i.debian.org/manual/">最新版本（Git）</a>
<br />
详细的安装步骤</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian 安装程序 FAQ</a> 和 <a href="$(HOME)/CD/faq/">Debian CD FAQ</a><br />
常见问题和解答</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian 安装程序 Wiki</a><br />
由[CN:社区:][HKTW:社群:]维护的文档</li>
</ul>

<h1>联系我们</h1>

<p>
<a href="https://lists.debian.org/debian-boot/">debian-boot
邮件列表</a> 是讨论 Debian 安装程序开发工作的主要论坛。
</p>

<p>
我们也有一个 IRC 频道，<tt>irc.debian.org</tt> 的 #debian-boot。这个\
频道主要用于开发，不过偶尔也会用于[CN:支持:][HKTW:支援:]。\
如果您没有收到回复，请尝试使用邮件列表。
</p>
