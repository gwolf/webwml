#use wml::debian::template title="建立一个 Debian 仓库的镜像"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/sid/archive.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="e466bedb14f9f661d1f4a77514cbe26596877486"

<toc-display />

<toc-add-entry name="whether">是否需要镜像</toc-add-entry>

<p>当我们赞扬新的镜像时，每个潜在的镜像维护者应确保在尝试开始建立自己的镜像之前可以回答以下\
问题：</p>

<ul>
  <li>我所在的位置是否需要镜像？也许附近已经有镜像了。</li>
  <li>我有资源托管镜像吗？镜像会占用大量<a href="size">磁盘空间</a>和带宽，因此必须承担一定\
      的成本。</li>
  <li>镜像是正确的选择吗？如果您主要想为您的 ISP 或机构的用户提供支持，那么诸如 \
      apt-cacher-ng、squid 或 varnish 之类的缓存代理可能是更好的选择。</li>
</ul>

<toc-add-entry name="what">镜像什么内容</toc-add-entry>

<p>在<a href="./">主镜像站点页</a>列出了可供镜像的仓库。</p>

<ul>
<li>
用户会寻找 debian /仓库，用以通过网络安装 Debian、（使用 jigdo）构建 CD 以及升级已安装的\
系统。<em>我们建议您镜像此存储库。</em></li>

<li>
debian-cd/ 是一个在所有不同的镜像服务器上都不相同的仓库。在某些站点上，它包含用于从中构建 \
CD 映像的 jigdo 模板（与 debian/ 中的文件结合使用）；在某些站点上，它包含已构建的 CD 映像；\
在某些站点上，两者都被包含。
<br />
请参阅<a href="$(HOME)/CD/mirroring/">镜像 CD 映像</a>的页面，以获取有关此内容的更多信息。</li>

<li>
debian-archive/ 包含 Debian 当前的<em>存档</em>、旧版本和过时版本。通常只有一小部分用户\
对此感兴趣。（如果您不知道是否要镜像，那么可能不需要。）
</li>

</ul>

<p>请参阅<a href="size">镜像大小</a>页面，以获取有关镜像大小的更准确的信息。</p>

<p>debian-security/ 仓库包含 Debian 安全团队发布的安全更新。虽然这听起来对每个人都很有趣，\
但是我们不建议我们的用户使用镜像来获取安全更新，而是要求他们直接从我们的分布式 \
security.debian.org 服务中下载更新。<em>我们<strong>不</strong>推荐镜像 debian-security。</em></p>

<toc-add-entry name="wherefrom">从哪建立镜像</toc-add-entry>

<p>请注意，<code>ftp.debian.org</code> 不是 Debian 软件包的规范存放位置，它仅是从内部 Debian \
服务器进行更新的几台服务器之一。

有许多支持 rsync 的<a href="list-full">公共镜像站点</a>，从这些站点进行镜像是很合适的。请\
从网络角度使用离您最近的站点。</p>

<p>您应该避免从会被解析为多个地址（例如 <code>ftp.us.debian.org</code>）的任何服务名称进行\
镜像，因为这可能会导致当您的上游镜像不同步时，您的同步镜像处于不同的状态。

#
另请注意，HTTP 是唯一一个我们保证存在于 <code>ftp.CC.debian.org</code> 的服务。如果要使用 \
rsync 镜像（建议使用 ftpsync），我们建议您为当前提供 <code>ftp.CC.debian.org</code> 的机器\
选择正确的站点名称。（请查看该服务器上的 <code>/debian/project/trace</code> 目录以了解这\
一点）

<toc-add-entry name="how">如何镜像</toc-add-entry>

<p>推荐的镜像方法是使用 ftpsync 脚本集，我们提供了两种方法供您获取它：</p>
<ul>
    <li>作为一个 tarball 从 <url "https://ftp-master.debian.org/ftpsync.tar.gz"> 下载</li>
    <li>作为一个 git 存储库： <kbd>git clone https://salsa.debian.org/mirror-team/archvsync.git</kbd> (请参阅 <url "https://salsa.debian.org/mirror-team/archvsync/">)</li>
</ul>

<p>请不要使用自己的脚本，也不要仅使用单遍 rsyncs。使用 ftpsync 可确保以某种方式完成更新，\
以便令 apt 不产生混乱。特别是，ftpsync 会按一定顺序处理翻译、内容和其他元数据文件，以便\
如果用户在镜像进行同步时更新软件包列表，apt 也不会出现验证错误。此外，它还会生成包含更多\
信息的跟踪文件，这些信息对于确定镜像是否正常工作、镜像同步的体系架构以及它从何处进行同步\
很有用。</p>

<toc-add-entry name="partial">部分镜像</toc-add-entry>

<p>考虑到<a href="size">Debian 仓库已经很大</a>，我们建议只镜像仓库的一部分。公共镜像应当\
包含所有套件（测试、不稳定等），但是它们可能会限制它们同步的体系架构集。ftpsync 的配置文件\
为此设有 ARCH_EXCLUDE 和 ARCH_INCLUDE 设置选项。</p>

<toc-add-entry name="when">何时镜像</toc-add-entry>

<p>主仓库每天更新四次。镜像通常在 3：00、9：00、15：00 和 21:00（所有时间均为 UTC）左右\
开始更新，但这些时间从来都不是固定的时间，因此您不应将您的镜像同步时间固定在这些时间上。</p>

<p>您的镜像应当在主仓库镜像更新之后几个小时进行更新。您应检查要镜像的站点是否在其 \
project/trace/ 子目录下保留了一个时间戳文件。该时间戳文件与该站点重名，会包含站点上次更新\
镜像的完成时间。为了安全起见，请在此完成时间上增加几个小时，然后进行镜像。</p>

<p><b>您的镜像与主仓库保持同步是至关重要的。</b>每24小时至少更新4次以确保您的镜像真实地\
反映了仓库。请理解这点：与主仓库文件不同步的镜像将不会在官方镜像列表中被列出。</p>

<p>实现每天自动运行镜像同步的最简单的方法是使用 cron。查看 <kbd>man crontab</kbd> 以获得\
详细信息。</p>

<p>请注意，如果您的站点是通过推送机制触发同步的，那您无需担心任何此类情况。</p>

<h3>Push-triggered 镜像</h3>

<p><q>推送</q>镜像是我们开发的一种镜像形式，可最大程度地减少仓库文件的更改同步到镜像站点\
所需的时间。上游镜像使用一个 SSH 触发器来告诉下游镜像自行更新。有关其工作原理、为什么它是\
安全的以及如何设置推送镜像的详细说明，请参见<a href="push_mirroring">完整说明</a>。</p>

<toc-add-entry name="settings">推荐的额外设置</toc-add-entry>

<p>公共镜像应该令 Debian 仓库文件在 <code>/debian</code> 下可以通过 HTTP 访问。</p>

<p>此外，请确保已启用目录列表（带有完整的文件名），并遵循符号链接。

如果您使用 Apache，如下配置应该能使用：
<pre>
&lt;Directory <var>/path/to/your/debian/mirror</var>&gt;
   Options +Indexes +SymlinksIfOwnerMatch
   IndexOptions NameWidth=* +SuppressDescription
&lt;/Directory&gt;
</pre>

<toc-add-entry name="submit">如何将一个镜像添加到镜像列表</toc-add-entry>

<p>
如果您想让自己的镜像在官方镜像列表中被列出，请：
</p>

<ul>
<li>确保您的镜像每24小时与仓库同步4次</li>
<li>确保您的镜像包含所同步的体系架构的源代码文件</li>
</ul>

<p>设置好镜像后，应<a href="submit">在 Debian 注册</a>，以使其被包含在\
<a href="list">官方镜像列表</a>。可以使用我们的<a href="submit">简单 web 表单</a>来提交。</p>


<p>如有任何问题或疑问，可以发送电邮到 <email mirrors@debian.org> 询问。</p>

<toc-add-entry name="mailinglists">邮件列表</toc-add-entry>

<p>有关于 Debian 镜像，存在两个公共<a href="../MailingLists/">邮件列表</a>： \
<a href="https://lists.debian.org/debian-mirrors-announce/">debian-mirrors-announce</a> 和\
<a href="https://lists.debian.org/debian-mirrors/">debian-mirrors</a>。
我们鼓励所有镜像维护者订阅公告列表，因为它会被用于发布任何重要的公告。此列表受到审核，订阅\
此列表只会收到少量邮件。第二个邮件列表用于一般性的讨论，向所有人开放。</p>

<p>如果您存在一些在这些网页上无法得到回答的问题，可以通过 <email mirrors@debian.org> 或\
使用在 <tt>irc.debian.org</tt> 上的 IRC 聊天室 #debian-mirrors 联系我们。</p>


<toc-add-entry name="private-mirror">专用（部分）镜像的注意事项</toc-add-entry>

<p>
如果您只想为自己的站点建立一个镜像，并且只需要同步一部分套件（例如稳定版），\
<a href="https://packages.debian.org/stable/debmirror">debmirror</a> 可能也很适合您。
</p>
