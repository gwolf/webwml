<define-tag pagetitle>Debian 10 <q>buster</q> publicado</define-tag>
<define-tag release_date>2019-07-06</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="cbfcd50b343ab08499a29ae770a5528eda687986" maintainer="Jose Miguel Parrella"

<p>Después de 25 meses de desarrollo, el proyecto Debian se complace
en presentar la nueva versión estable 10 (nombre en clave <q>buster</q>),
a la que se dará soporte durante los próximos 5 años gracias al trabajo combinado de los
equipos de <a href="https://security-team.debian.org/">seguridad</a> 
y <a href="https://wiki.debian.org/LTS">soporte a largo plazo</a> de Debian.
</p>

<p>
Debian 10 <q>buster</q> se publica con varios entornos y aplicaciones para el
escritorio. Debian ahora incluye, entre otros:
</p>
<ul>
<li>Cinnamon 3.8,</li>
<li>GNOME 3.30,</li>
<li>KDE Plasma 5.14,</li>
<li>LXDE 0.99.2,</li>
<li>LXQt 0.14,</li>
<li>MATE 1.20,</li>
<li>Xfce 4.12.</li>
</ul>

<p>
En esta versión GNOME utiliza de forma predeterminada el servidor de gráficos
Wayland en vez de Xorg. Wayland tiene un diseño más simple y moderno con más
ventajas de seguridad. Sin embargo, Xorg aún se instala de forma predeterminada
y el gestor de sesiones permitirá a los usuarios elegir Xorg para su siguiente
sesión.
</p>

<p>
Gracias al proyecto de Compilación Reproducible, más del 91% de los
paquetes fuentes incluidos en Debian compilarán paquetes binarios
idénticos bit-a-bit.
Esta es una función de verificación importante que protege a los usuarios
de posibles intentos maliciosos de alterar los compiladores y sistemas de
compilación. En futuras versiones de Debian se incluirán las herramientas y
los meta-datos necesarios para que los usuarios puedan validar
el origen de los paquetes dentro del archivo.
</p>

<p>
Para aquellos usuarios en ambientes sensibles a la seguridad, el sistema de control
de acceso mandatorio AppArmor, que restringe lo que los programas pueden hacer, está
instalado y activado de forma predeterminada.
Más aún, todos los métodos de transporte provistos por APT (excepto cdrom, gpgv y rsh)
pueden ahora usar contenciones <q>seccomp-BPF</q>.
El método https para APT ahora es parte del paquete apt y no es necesario instalarlo
de forma separada.
</p>

<p>
En Debian 10 <q>buster</q>, se utiliza de forma predeterminada el
marco de trabajo nftables para filtrado de redes. A partir de iptables v1.8.2,
el paquete binario incluye iptables-nft e iptables-legacy, dos variantes
de la interfaz de línea de órdenes de iptables. La variante basada en
nftables usa el subsistema nf_tables, parte del núcleo Linux. Se puede utilizar
el mecanismo de <q>alternatives</q> para escoger la variante deseada.
</p>

<p>
El soporte UEFI (<q>Unified Extensible Firmware Interface</q>, «Interfaz de
Firmware Extensible Unificada») introducido en Debian 7 <q>wheezy</q> ha mejorado en Debian 10 <q>buster</q>. En esta
versión se incluye soporte para «arranque seguro» («Secure Boot») en las arquictecturas amd64, i386 y arm64
y debería funcionar sin modificación en la mayoría de los sistemas que disponen de
«arranque seguro». Esto significa que los usuarios ya no tendrían que desactivar el «arranque seguro»
en la configuración de su firmware.
</p>

<p>
Los paquetes cups y cups-filters vienen instalados en Debian 10 <q>buster</q>,
dándole a los usuarios lo que necesitan para imprimir sin controladores. Las 
impresoras en red y las impresoras IPP serán configuradas y administradas de
forma automática por cups-browsed, prescindiendo del uso de los controladores o 
componentes no libres que proporciona el fabricante.
</p>

<p>
Más del 62% de los paquetes han sido actualizados en Debian 10 <q>buster</q>, que 
incluye nuevas versiones de programas como:
</p>
<ul>
<li>Apache 2.4.38</li>
<li>BIND DNS Server 9.11</li>
<li>Chromium 73.0</li>
<li>Emacs 26.1</li>
<li>Firefox 60.7 (en el paquete firefox-esr)</li>
<li>GIMP 2.10.8</li>
<li>GNU Compiler Collection 7.4 y 8.3</li>
<li>GnuPG 2.2</li>
<li>Golang 1.11</li>
<li>Inkscape 0.92.4</li>
<li>LibreOffice 6.1</li>
<li>Linux 4.19.x</li>
<li>MariaDB 10.3</li>
<li>OpenJDK 11</li>
<li>Perl 5.28</li>
<li>PHP 7.3</li>
<li>PostgreSQL 11</li>
<li>Python 3 3.7.2</li>
<li>Ruby 2.5.1</li>
<li>Rustc 1.34</li>
<li>Samba 4.9</li>
<li>systemd 241</li>
<li>Thunderbird 60.7.2</li>
<li>Vim 8.1</li>
<li>y más de 59.000 paquetes listos para utilizar, construidos a partir de más de 29.000 paquetes fuente.</li>
</ul>

<p>
Con esta amplia selección de software y su amplio soporte de arquitecturas,
Debian se mantiene fiel a su objetivo de ser el sistema operativo universal. Es
una elección apropiada para muchos casos de uso: desde sistemas de escritorio
hasta netbooks; entornos de desarrollo y clústeres; y para servidores web,
de almacenamiento y de bases de datos. De igual forma, el trabajo de control de calidad
que incluye pruebas automáticas de instalación y actualización para todos los
paquetes en el archivo de Debian ayuda a que <q>buster</q> satisfaga las altas 
expectativas que los usuarios tienen de una nueva versión de Debian.
</p>

<p>
En total, se proporciona soporte a diez arquitecturas:
PC de 64 bits / Intel EM64T / x86-64 (<code>amd64</code>),
PC de 32 bits / Intel IA-32 (<code>i386</code>),
Motorola/IBM PowerPC de 64 bits little-endian  (<code>ppc64el</code>),
IBM S/390 de 64 bits (<code>s390x</code>),
para ARM, <code>armel</code>
y <code>armhf</code> para hardware de 32 bits tanto antiguo como más reciente,
además de <code>arm64</code> para la arquitectura de 64 bits <q>AArch64</q>,
y para MIPS, <code>mips</code> (big-endian) 
y <code>mipsel</code> (little-endian) en cuanto a 32 bits 
y <code>mips64el</code> para el hardware little-endian de 64 bits.
</p>

<h3>¿Quiere probarlo?</h3>
<p>
Si simplemente quiere probar Debian 10 <q>buster</q> sin instalarlo,
puede utilizar una de las <a href="$(HOME)/CD/live/">imágenes en vivo («live»)</a> que cargan y ejecutan
el sistema operativo en modo de sólo lectura en la memoria de su ordenador.
</p>

<p>
Estas imágenes se ofrecen para las arquitecturas <code>amd64</code> y
<code>i386</code> para su instalación en DVD, memorias USB e instalaciones netboot.
Los usuarios pueden elegir qué entornos de escritorio probar: Cinnamon, GNOME, KDE Plasma,
MATE, Xfce y, como novedad en buster, LXQt. Debian Live Buster vuelve a ofrecer la imagen
«live» estándar que permite probar un sistema Debian sin interfaz gráfica.
</p>
<p>
Si, en cambio, desea instalar la imagen en el disco duro de su ordenador, tiene la
opción de hacerlo desde la imagen en vivo que incluye el instalador independiente
Calamares además del estándar (instalador de Debian). Hay más información disponible en las
<a href="$(HOME)/releases/buster/releasenotes">Notas de publicación</a> y en la 
<a href="$(HOME)/CD/live/">sección de las imágenes de instalación en vivo</a>
del sitio web de Debian.
</p>

<p>
Para instalar Debian 10 <q>buster</q> directamente en el disco duro puede elegir
otros medios de instalación tales como discos Blu-ray, DVD, CD, memorias USB o a través
de la red.
Puede instalar varios entornos de escritorio como Cinnamon, GNOME, KDE Plasma, LXDE,
LXQt, MATE o Xfce con estas imágenes.
Además, los CD de <q>múltiples arquitecturas</q> permiten instalar varias arquictecturas
desde un mismo medio de instalación. También puede crear medios de instalación USB
autoarrancables (vea la <a href="$(HOME)/releases/buster/installmanual">Guía de instalación</a>
para más detalles).
</p>

<p>
Para usuarios de la nube, Debian ofrece soporte directo para muchas
plataformas de nube. Hay imágenes oficiales de Debian que se pueden escoger
en las galerías de los proveedores. Debian también publica <a
href="https://cloud.debian.org/images/openstack/current/">imágenes pre-construidas para OpenStack</a>
para <code>amd64</code> y <code>arm64</code>, que puede descargar y usar en una nube local.
</p>

<p>
Debian se puede instalar en 76 idiomas distintos, la mayoría de ellos disponibles tanto en
interfaces de texto como gráficas.
</p>

<p>
Las imágenes de instalación se pueden descargar a través de:
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (recomendado),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a>, o
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; vea
<a href="$(HOME)/CD/">Debian en CD</a> para más información. <q>Buster</q>
estará físicamente disponible en DVD, CD-ROM, y discos de Blu-ray a través de
nuestros <a href="$(HOME)/CD/vendors">vendedores</a>.
</p>


<h3>Actualizando Debian</h3>
<p>
La herramienta de gestión de paquetes apt maneja las actualizaciones
a Debian 10 desde la versión anterior, Debian 9 (<q>stretch</q>).
Debian puede actualizarse in situ, sin problemas o tiempo de inactividad
pero se recomienda encarecidamente leer las <a href="$(HOME)/releases/buster/releasenotes">Notas de publicación</a>
y la <a href="$(HOME)/releases/buster/installmanual">Guía de instalación</a>
para informarse de posibles problemas y obtener instrucciones detalladas para instalar
y actualizar Debian. En las próximas semanas, las Notas de instalación continuarán
editándose y traduciéndose a más idiomas.
</p>


<h2>Acerca de Debian</h2>

<p>
El proyecto Debian fue fundado en 1993 por Ian Murdock para ser un proyecto comunitario 
verdaderamente libre. Desde entonces el proyecto ha crecido hasta ser uno de 
los proyectos más grandes e importantes de software libre. Miles de voluntarios 
de todo el mundo trabajan juntos para crear y mantener programas para Debian. 
Se encuentra traducido a 70 idiomas y soporta una gran cantidad de arquitecturas 
de ordenadores, por lo que el proyecto se refiere a sí mismo como 
<q>el sistema operativo universal</q>. 
</p>

<h2>Información de contacto</h2>

<p>Para más información, visite la página web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a> o envíe un correo
electrónico a &lt;press@debian.org&gt;.</p>
