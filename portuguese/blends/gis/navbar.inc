#use wml::debian::blends::gis

{#alternate_navbar#:
   <div id="second-nav">
      <p><a href="$(HOME)/blends/gis/">Blend&nbsp;GIS&nbsp;do&nbsp;Debian</a></p>
    <ul>
      <li><a href="$(HOME)/blends/gis/about">Sobre</a></li>
      <li><a href="$(HOME)/blends/gis/contact">Contato</a></li>
      <li><a href="$(HOME)/blends/gis/get/">Obtendo&nbsp;o&nbsp;blend</a>
         <ul>
         <li><a href="$(HOME)/blends/gis/get/live">Download&nbsp;de&nbsp;imagens&nbsp;live</a></li>
         <li><a href="$(HOME)/blends/gis/get/metapackages">Usando&nbsp;os&nbsp;metapacotes</a></li>
	 </ul>
      </li>
      <li><a href="$(HOME)/blends/gis/support">Suporte</a></li>
      <li><a href="$(HOME)/blends/gis/deriv">Derivados</a></li>
      <li><a href="https://wiki.debian.org/DebianGIS#Development">Desenvolvimento</a>
        <ul>
        <li><a href="<gis-policy-html/>">Políticas do time GIS</a></li>
        </ul>
      </li>
      <li><a href="https://twitter.com/DebianGIS"><img src="$(HOME)/blends/gis/Pics/twitter.gif" alt="Twitter" width="80" height="15"></a></li>
    </ul>
   </div>
:#alternate_navbar#}
