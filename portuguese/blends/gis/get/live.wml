#use wml::debian::blend title="Download de imagens live"
#use wml::debian::blends::gis
#use "../navbar.inc"
#use wml::debian::translation-check translation="a2a93c6af14d9de0d3c3aa2b2d7fa4d06a48ee43"

<p>O Pure Blend GIS do Debian produz <b>imagens live em DVD</b> que podem
ser usadas para testar o Pure Blend GIS do Debian em um computador sem
ter que instalá-lo primeiro. As imagens também contém um instalador que pode
ser usado para instalar o Debian junto com os pacotes do blend.</p>

<h2>Blend GIS do Debian <em>estável (stable)</em></h2>

<p>Um lançamento prévio do Blend GIS do Debian em DVD live está disponível
para download.</p>

<p>O último lançamento estável é: <strong><stable-version/></strong>.</p>

<ul>
        <li><a href="<stable-amd64-url/>">imagem live amd64 em DVD (ISO)</a>
        <li><a href="<stable-i386-url/>">imagem live i386 em DVD (ISO)</a>
        <li><a href="<stable-source-url/>">arquivo-fonte da imagem live em DVD (tar)</a>
</ul>

<p>Para imagens webboot, checksums e assinaturas GPG, veja a <a
href="https://cdimage.debian.org/cdimage/blends-live/">lista com todos
os arquivos</a>.</p>

<h2>Blend GIS do Debian <em>teste (testing)</em></h2>

<p>Em um futuro próximo, DVDs live serão construídos para o stretch (a atual
versão teste (testing) do Debian), embora eles não estejam disponíveis
atualmente.</p>

<h2>Começando</h2>

<h3>Usando um DVD</h3>

<p>A maior parte dos modernos sistemas operacionais serão capazes de queimar
imagens ISO em mídias DVD. O FAQ do Debian em CD fornece instruções para
queimar imagens ISO usando
<a href="https://www.debian.org/CD/faq/index#record-unix">Linux</a>, <a
href="https://www.debian.org/CD/faq/index#record-windows">Windows</a> e <a
href="https://www.debian.org/CD/faq/index#record-mac">Mac OS</a>. Se você
está tendo dificuldades, um motor de busca na web deve fornecer as
respostas que você precisa.</p>

<h3>Usando um pendrive USB</h3>

<p>As imagens ISO são construídas como imagens híbridas, de modo que você possa
copiá-las diretamente para um pendrive USB sem usar qualquer software especial
como o unetbootin. Em um sistema Linux, você pode fazê-lo dessa forma:</p>

<pre>sudo dd if=/caminho/para/imagem-live-do-debian-gis.iso of=/dev/sd<b>X</b></pre>

<p>A saída do comando dmesg deve permitir que você veja o nome do dispositivo
do pendrive USB e, com a letra fornecida, você precisará alterar <b>X</b>.</p>
