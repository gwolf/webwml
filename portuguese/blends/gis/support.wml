#use wml::debian::blend title="Suporte para o blend"
#use "navbar.inc"
#use wml::debian::translation-check translation="ac688ebb94c4334be47e6f542320d90001163462"

<h2>Documentação</h2>

<p>Antes de procurar pela ajuda de alguém, é usualmente bom tentar
encontrar uma resposta para seu problema por você mesmo(a). Dessa maneira,
você geralmente vai conseguir a resposta que precisa, e mesmo que não consiga,
a experiência de ter lido a documentação provavelmente será útil para você
no futuro.</p>

<p>Veja a <a href="../../doc/">página de documentação do Debian</a> para uma
lista da documentação disponível.</p>

<h2>Listas de discussão</h2>

<p>O Debian é desenvolvido através de desenvolvimento distribuído por todo o
mundo. Portanto, o e-mail é a forma preferida para discussão de vários assuntos.
Muitas das conversas entre desenvolvedores(as) Debian e usuários(as) são
administradas através de diversas listas de discussões.</p>

# Tradutores(as): por favor, troque para a lista de seu idioma.

<p>Para suporte geral ao Debian em português, você pode usar a <a
href="https://lists.debian.org/debian-user-portuguese/">lista de discussão de usuários(as) do Debian</a>.</p>

<p>Para suporte ao(à) usuário(a) em outras línguas, por favor verifique o <a
href="https://lists.debian.org/users.html">índice de listas de discussão para
usuários(as)</a>.</p>

<h2>Ajuda on-line em tempo real usando IRC</h2>

<p><a href="http://www.irchelp.org/">O IRC (Internet Relay Chat) </a> é uma
forma de conversar com pessoas de todo o mundo em tempo
real. Canais IRC dedicados ao Debian podem ser encontrados no
<a href="https://www.oftc.net/">OFTC</a>.</p>

<p>Para conectar você precisa de um cliente IRC. Alguns dos clientes mais
populares são
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> e
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>,
todos eles são empacotados para o
Debian. Uma vez que você tenha instalado o cliente, você precisa informá-lo
para que conecte com o servidor. Na maioria dos clientes, você pode fazer isso
digitando:</p>

<pre>
/server irc.debian.org
</pre>

<p>Uma vez que esteja conectado(a), junte-se ao canal <code>#debian-user</code>,
digitando</p>

<pre>
/join #debian-user
</pre>

<p>Nota: clientes como HexChat frequentemente têm uma interface gráfica
diferente para o(a) usuário(a) entrar em servidores/canais.</p>

<p>Também existem vários outras redes IRC onde você pode bater um papo sobre o
Debian. Uma das mais proeminentes é a
<a href="https://freenode.net/">rede de IRC freenode</a> em
<kbd>chat.freenode.net</kbd>.</p>
