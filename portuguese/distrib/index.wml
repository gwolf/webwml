#use wml::debian::template title="Obtendo o Debian"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="fc74a202c0f1463d4bc08c88f3da3c2b4f1066f1"

<p>O Debian é distribuído <a href="../intro/free">livremente</a>
pela Internet. Você pode baixá-lo integralmente partir de qualquer um dos nossos
<a href="ftplist">espelhos</a> (<q>mirrors</q>).
O <a href="../releases/stable/installmanual">Manual de Instalação</a>
contém instruções detalhadas da instalação e as notas de lançamento podem ser
encontradas <a href="../releases/stable/releasenotes">aqui</a>.</p>

<p>Se você quer simplesmente instalar o Debian, estas são suas opções:</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Baixe uma imagem de instalação</a></h2>
    <p>Dependendo da sua conexão de Internet, você pode baixar qualquer uma
    das seguintes opções:</p>
    <ul>
      <li>Uma <a href="netinst"><strong>pequena imagem de instalação</strong></a>:
	    pode ser baixada rapidamente e deve ser gravada em um disco removível.
	    Para usar isso, você precisará de uma máquina com uma conexão de Internet.
	<ul class="quicklist downlist">
          <li><a title="Baixe o instalador para PC Intel e AMD de 64 bits"
                 href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">iso netinstall para PC de 64 bits</a></li>
          <li><a title="Baixe o instalador para PC normal Intel e AMD de 32 bits"
                 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">iso netinstall para PC de 32 bits</a></li>
       </ul>
      </li>
      <li>Uma imagem <a href="../CD/"><strong>maior de instalação
	completa</strong></a>: contém mais pacotes, tornando mais fácil de instalar
	em máquinas sem uma conexão de Internet.
	<ul class="quicklist downlist">
	  <li><a title="Baixe torrents de DVD para PC Intel e AMD de 64 bits"
	         href="<stable-images-url/>/amd64/bt-dvd/">torrents para PC de 64 bits (DVD)</a></li>
	  <li><a title="Baixe torrents de DVD para PC normal Intel e AMD de 32 bits"
		 href="<stable-images-url/>/i386/bt-dvd/">torrents para PC de 32 bits (DVD)</a></li>
	  <li><a title="Baixe torrents de CD para PC Intel e AMD de 32 bits"
	         href="<stable-images-url/>/amd64/bt-cd/">torrents para PC de 64 bits (CD)</a></li>
	  <li><a title="Baixe torrents de CD para PC normal Intel e AMD de 32 bits"
		 href="<stable-images-url/>/i386/bt-cd/">torrents para PC de 32 bits (CD)</a></li>
        </ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Use uma imagem do Debian na nuvem</a></h2>
    <ul>
      <li>Uma <a href="https://cloud.debian.org/images/cloud/"><strong>imagem oficial na nuvem</strong></a>:
            pode ser usada diretamente no seu provedor de nuvem, construído pelo Time do Debian Cloud.
        <ul class="quicklist downlist">
          <li><a title="Imagem OpenStack para Qcow2 Intel e AMD de 64 bits" href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-amd64.qcow2">AMD/Intel OpenStack (Qcow2) para 64 bits</a></li>
          <li><a title="Imagem OpenStack para Qcow2 ARM de 64 bits" href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-arm64.qcow2">ARM OpenStack (Qcow2) para 64 bits</a></li>
        </ul>
      </li>
    </ul>
    <h2><a href="../CD/live/">Experimente o Debian Live antes de instalar</a></h2>
    <p>
	Você pode experimentar o Debian inicializando um sistema Live a partir de um
	CD, DVD ou pendrive USB sem instalar nenhum arquivo no computador. Quando
	você estiver pronto, você pode executar o instalador incluído (a partir
	do Debian 10 Buster, ele é o amigável ao usuário final
	<a href="https://calamares.io">instalador Calamares</a>).
	Desde que as imagens atendam aos seus requisitos de tamanho, idioma e
	seleção de pacotes, este método pode ser adequado para você.
	Leia mais <a href="../CD/live#choose_live">informações sobre este método</a>
	para ajudar você a decidir.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Baixe torrents Live para PC Intel e AMD de 64 bits"
	     href="<live-images-url/>/amd64/bt-hybrid/">torrents Live para PC de 64 bits</a></li>
      <li><a title="Baixe torrents Live para PC normal Intel e AMD de 32 bits"
	     href="<live-images-url/>/i386/bt-hybrid/">torrents Live para PC de 32 bits</a></li>
    </ul>
  </div>
</div>
 <div class="line">
  <div class="item col50">
   <h2><a href="../CD/vendors/">Compre um conjunto de CDs ou DVDs de um dos
     vendedores de CDs Debian</a></h2>

   <p>
      Muitos dos vendedores vendem a distribuição por menos que US$5 mais
      o envio (confira as páginas web deles para ver se eles enviam
      internacionalmente).
      <br />
      Alguns dos <a href="../doc/books">livros sobre Debian</a> também vêm com
      CDs.
   </p>

   <p>Aqui estão as vantagens básicas dos CDs:</p>

   <ul>
     <li>A instalação a partir de um conjunto de CDs é mais direta.</li>
     <li>Você pode instalar em máquinas sem conexão com a Internet.</li>
         <li>Você pode instalar o Debian (em quantas máquinas você quiser) sem
         ter que baixar todos os pacotes por conta própria.</li>
     <li>O CD pode ser usado para recuperar, mais facilmente, um sistema
         Debian danificado.</li>
   </ul>
  </div>
  <div class="item col50 lastcol">
   <h2><a href="pre-installed">Compre um computador com Debian
     pré-instalado</a></h2>
   <p>Há algumas vantagens nisso:</p>
   <ul>
    <li>Você não tem que instalar o Debian.</li>
    <li>A instalação é pré-configurada para seu hardware.</li>
    <li>O fornecedor pode prover suporte técnico.</li>
   </ul>
  </div>
</div>
