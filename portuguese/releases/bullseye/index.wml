#use wml::debian::template title="Informações de lançamento do Debian &ldquo;bullseye&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="c8265f664931f5ce551446ba751d63d63b70ce86"

<if-stable-release release="bullseye">

<p>O Debian <current_release_bullseye> foi lançado em <a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>.
<ifneq "11.0" "<current_release>"
  "Debian 11.0 foi lançado inicialmente em <:=spokendate('XXXXXXXX'):>."
/>
O lançamento inclui muitas grandes mudanças descritas em
nosso <a href="$(HOME)/News/XXXX/XXXXXXXX">comunicado à imprensa</a> e
nas <a href="releasenotes">Notas de lançamento</a>.</p>

#<p><strong>O Debian 11 foi substituído pelo
#<a href="../lenny/">Debian 12 (<q>bookworm</q>)</a>.
#Atualizações de segurança foram descontinuadas em <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### Este parágrafo é orientativo, revise antes de publicar!
#<p><strong>Contudo, o bullseye se beneficia do suporte de longo prazo (LTS -
#Long Term Support) até o final de xxxxx 20xx. O LTS é limitado ao i386, amd64,
#armel, armhf e arm64.
#Todas as outras arquiteturas não são mais suportadas no buster.
#Para mais informações, consulte a <a
#href="https://wiki.debian.org/LTS">seção LTS da wiki do Debian</a>.
#</strong></p>

<p>Para obter e instalar o Debian, consulte
a página de <a href="debian-installer/">informação de instalação</a> e o
<a href="installmanual">Manual de Instalação</a>. Para atualizar a partir de uma
versão mais antiga do Debian, consulte as instruções nas
<a href="releasenotes">notas de lançamento</a>.</p>

### Activate the following when LTS period starts.
#<p>Architectures supported during Long Term Support:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>As seguintes arquiteturas de computadores são suportadas nesta versão:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Apesar de nossos desejos, podem existir alguns problemas nesta versão,
embora ela tenha sido declarada estável (stable). Nós fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode <a href="reportingbugs">relatar outros problemas</a>
para nós.</p>

<p>Por último mas não menos importante, nós temos uma lista de <a href="credits">pessoas
que recebem crédito</a> por fazerem este lançamento acontecer.</p>
</if-stable-release>

<if-stable-release release="buster">

<p>O codinome para o próximo grande lançamento do Debian após o  <a
href="../buster/">buster</a> é <q>bullseye</q>.</p>

<p>Esta versão iniciou como uma cópia da buster e atualmente está em um estado
chamado <q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">teste (testing)</a></q>.
Isso significa que as coisas não devem quebrar de forma crítica quanto nas
verões instável (unstable) ou experimental, pois os pacotes podem entrar nesta
versão somente após um certo período de tempo e quando
não possui nenhuma menção de bug crítico em seus lançamentos.</p>

<p>Observe que as atualizações de segurança para a versão teste (testing) ainda são
<strong>not</strong> gerenciados pela equipe de segurança. Portanto, pode ser que
o<q>testing</q> <strong>não</strong> obtenha atualizações de segurança em tempo hábil.
# Para maiores informações, favor consultar o
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">anúncio</a>
# do Time de Segurança do Testing.
Incentivamos que você altere as
entradas do arquivo sources.list de testing para buster por enquanto, caso
necessite de suporte de segurança. Consulte também o
<a href="$(HOME)/security/faq#testing">FAQ do Time de Segurança</a> da
versão teste (testing).</p>

<p>Pode haver um <a href="releasenotes">esboço das notas de versão disponíveis.</a>.
Por favor, <a href="https://bugs.debian.org/release-notes">verifique também as
adições propostas às notas de versão</a>.</p>

<p>Para imagens de instalação e documentação sobre como instalar o <q>testing</q>,
consulte <a href="$(HOME)/devel/debian-installer/">a página do instalador do Debian</a>.</p>

<p>Para encontrar mais informações sobre com a versão teste (testing) funciona, consulte
<a href="$(HOME)/devel/testing">as informações dos(as) desenvolvedores(as) sobre isso</a>.</p>

<p>As pessoas costumam perguntar se existe um único <q>medidor de progresso</q>
de liberação do lançamento. Infelizmente não há, mas podemos encaminhá-lo para vários sites
que descrevem as coisas que precisam ser tratadas para que o lançamento ocorra:</p>

<ul>
  <li><a href="https://release.debian.org/">Página de estado de lançamento genérico</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Bugs críticos de lançamento</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Bugs do sistema base</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Erros nos pacotes padrões e de tarefas</a></li>
</ul>

<p>Além disso, os relatórios de estado  geral são lançados pelo gerenciador de lançamentos
na <a href="https://lists.debian.org/debian-devel-announce/">\
lista de discussão sobre anúncios do desenvolvimento do Debian</a>.</p>

</if-stable-release>
