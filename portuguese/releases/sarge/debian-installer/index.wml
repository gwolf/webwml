#use wml::debian::template title="Debian &ldquo;sarge&rdquo; -- Informações de instalação" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/sarge/release.data"
#use wml::debian::translation-check translation="5011f532637dc7820b79b151eecfda4ab65aa22f"

<h1>Instalando o sarge</h1>

<p><strong>O Debian GNU/Linux 3.1 foi substituído pelo
<a href="../../lenny/">Debian GNU/Linux 4.0 (<q>etch</q>)</a>. Algumas
destas imagens de instalação podem não estar disponíveis, ou podem não
funcionar mais, é recomendado que você instale o etch em vez disso.
</strong></p>

<p>
<strong>Para instalar o Debian GNU/Linux</strong> <current_release_sarge>
(<em>sarge</em>), baixe qualquer uma das seguintes imagens:
</p>

<div class="line">
<div class="item col50">
<p><strong>
imagem de CD netinst (100 Mb)
</strong></p>
<netinst-images />
</div>

<div class="item col50 lastcol">
<p><strong>
imagem de CD businesscard (50 Mb)
</strong></p>
<businesscard-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>conjuntos completos de CDs</strong></p>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<p><strong>conjuntos completos de DVDs</strong></p>
<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>conjuntos completos de CDs (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>conjuntos completos de DVDs (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>
outras imagens (netboot, pendrive usb, disquete, fita, etc)
</strong></p>
<other-images />
</div>
</div>

<h1>Documentação</h1>

<p>
<strong>Se você lê somente um documento</strong> antes da instalação, leia
nosso <a href="../i386/apa">Howto de instalação</a>, um rápido passo a passo
do processo de instalação. Outras documentações úteis incluem:
</p>

<ul>
<li><a href="../installmanual">Guia de instalação do sarge</a><br />
instruções detalhadas de instalação</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">FAQ do instalador do
Debian</a> e <a href="$(HOME)/CD/faq/">FAQ do Debian-CD</a><br />
perguntas comuns e respostas</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
documentação mantida pela comunidade</li>
</ul>

<h1 id="errata">Errata</h1>

<p>
Esta é uma lista de problemas conhecidos no instalador que acompanha o
Debian GNU/Linux <current_release_sarge>. Se você teve algum problema
instalando o Debian e não vê seu problema listado aqui, por favor, envie-nos um
<a href="$(HOME)/releases/stable/i386/ch05s04.html#submit-bug">relatório de
instalação</a> descrevendo o problema ou
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">verifique a
wiki</a> para outros problemas conhecidos.
</p>

<h3 id="errata-r8">Errata para a versão 3.1r8</h3>

<p>
No changes from release 3.1r7.
</p>

<h3 id="errata-r7">Errata para a versão 3.1r7</h3>

<p>
O problema listado no 3.1r6 referente ao suporte à instalação do
<q>oldstable</q> a partir dos espelhos de rede foi resolvido. Todos os CDs de
instalação do 3.1r7 e outras imagens do instalador suportam totalmente a
instalação do sarge novamente.
</p><p>
A instalação da tarefa <q>ambiente de área de trabalho</q> pode falhar ao
instalar a partir de um CD completo. Isso se deve ao fato de o OpenOffice não
estar incluído no primeiro CD, além disso alguns pacotes de atualizações de
segurança do openoffice.org estão disponíveis nos espelhos de segurança.<br />
O problema pode ser evitado escolhendo também a opção <q>seleção manual de
pacotes</q> ao selecionar a tarefa da área de trabalho. Depois disso, o
<tt>aptitude</tt> exibirá os pacotes a serem instalados. Localize o pacote
<tt>openoffice.org-bin</tt> e desmarque-o pressionando a tecla <q>-</q> e
em seguida continue a instalação pressionando a tecla <q>g</q>.< br />
O problema também pode ser evitado com a adição de um espelho de rede ou com a
varredura do segundo CD completo durante a configuração do Apt.
</p>

<h3 id="errata-r6">Errata para a versão 3.1r6</h3>

<p>
O problema com a seleção do kernel no 3.1r5 foi resolvido nesta versão.
</p><p>
Devido ao fato do sarge não ser mais o <q>estável (stable)</q>, mas agora ser o
<q>oldstable</q>, o instalador não irá mais buscar pacotes corretamente nos
espelhos de rede.
Isso afeta mais as instalações que usam um espelho de rede antes da primeira
reinicialização, como imagens netboot e businesscard. Você ainda pode instalar
o sarge usando o netinst e imagens completas de CD ou DVD, desde que edite
manualmente o arquivo <tt>/etc/apt/sources.list</tt> para garantir que qualquer
linha da origem dos espelhos da rede aponte para <q>sarge</q> e <em>não</em>
para <q>stable</q> antes de instalar pacotes a partir de um espelho de rede.
Efetivamente, isso significa que você <em>não pode</em> usar um espelho de rede
durante a instalação!
</p>

<h3 id="errata-r5">Errata para a versão 3.1r5</h3>

<p>
Em quatro arquiteturas, o instalador pode selecionar um kernel incorreto para
o seu sistema. As arquiteturas afetadas são: i386, hppa, ia64 e s390.<br />
Você pode contornar esse problema inicializando o instalador com o parâmetro
<tt>debconf/priority=medium</tt>. Isso resultará na exibição de uma lista
completa de kernels disponíveis, a partir da qual você pode selecionar
manualmente o tipo adequado ao seu sistema.
</p>

<h3 id="errata-r4">Errata para a versão 3.1r4</h3>

<p>
O lançamento pontual 3.1r4 não inclui uma atualização do instalador, exceto
que o problema introduzido no 3.1r3 para sparc32 foi corrigido nessa versão.
Os comentários gerais listados abaixo para 3.1r3 ainda são válidos.
</p>

<h3 id="errata-r3">Errata para a versão 3.1r3</h3>

<p>
Além da errata original do sarge 3.1r0 listada abaixo, os seguintes problemas
devem ser observados na atualização do instalador com a versão 3.1r3 (r1 e r2
não incluíram uma atualização do instalador).

</p>

<p>
With the update of the kernel for this release, some of the installer images
originally released with sarge 3.1r0 will no longer work due to a mismatch
between the kernel the installer boots and kernel udebs loaded later. Affected
are all installation methods that download udebs from the network, e.g.
floppy-based and netboot installations. The updated installer images should
work correctly.<br>

Com a atualização do kernel para esta versão, algumas das imagens do instalador
originalmente lançadas com o sarge 3.1r0 não funcionarão mais devido a uma
incompatibilidade entre o kernel que o instalador inicializa e os udebs do
kernel carregados posteriormente. Os afetados são todos os métodos de
instalação que baixam udebs da rede, por exemplo instalações baseadas em
disquete e netboot. As imagens atualizadas do instalador devem funcionar
corretamente.<br>
As instalações baseadas em CD <b>não</b> são afetadas, portanto você pode
continuar usando imagens de CD/DVD de versões anteriores do sarge.
</p>

<ul>
	<li><b>A instalação do netboot no sparc32 está quebrada.</b>
        Devido a problemas com a geração de udebs do kernel, alguns módulos
        necessários para instalações do netboot estão ausentes nas imagens.
        Outros métodos de instalação podem ter problemas semelhantes.
	</li>
</ul>

<h3 id="errata-r0">Errata para a versão 3.1r0</h3>

<ul>
	<li><b>Iniciar com vga= fará com que a instalação do lilo falhe.</b>
        Se você precisar passar um parâmetro vga= para o kernel ao inicializar
        o instalador, não escolha instalar o lilo. A instalação do grub
        funcionará bem. Esse problema está corrigido nas compilações diárias do
        instalador.
	</li>

	<li><b>O JFS está quebrado no ia64.</b>
	O sistema de arquivos JFS está quebrado ao instalar o ia64 usando o
	kernel 2.6 (padrão). Evite usar o JFS no ia64 ou use o kernel 2.4.
	</li>

	<li><b>Tela vermelha na inicialização.</b>
	Às vezes o instalador pode aparecer com uma tela vermelha na
	inicialização ao usar o kernel 2.6. A instalação funciona bem, não há
	erro, mas o fundo da tela está com a cor errada, vermelho, em vez de
	azul. Esse é o bug #<a href="https://bugs.debian.org/273192">273192</a>.
	</li>

	<li><b>O driver SATA pode bloquear o acesso à unidade de CD nas
	instalações a partir de CD.</b>
	Em sistemas com um controlador IDE SATA que também possui a unidade de
	CD conectada, você pode ver o instalador travando durante a detecção de
	hardware da unidade de CD ou deixando de ler o CD logo em seguida.
	Um possível motivo é que o driver SATA (ata_piix e talvez outros)
	esteja bloqueando o acesso à unidade de CD.<br>
	Você pode tentar contornar esse problema inicializando o instalador no
	modo especialista e, na etapa "Detectar e montar CD-ROM", selecionando
	apenas os drivers necessários para o suporte ao CD. Estes são
	(ide-)genéricos, ide-cd e isofs.<br>
	Os drivers necessários para acessar o disco ainda serão carregados,
	mas em etapa posterior. Ao carregar os drivers de CD antes do driver
	SATA dessa maneira, você poderá concluir a instalação. Observe que o
	acesso ao CD-ROM ainda pode ser um problema após a reinicialização no
	sistema instalado.
	</li>

	<li><b>O instalador falha ao inicializar em alguns sistemas HP
	PA-RISC.</b>
	Em alguns sistemas, o ramdisk configurado por padrão para o instalador
	é muito pequeno. A inicialização do instalador com o parâmetro
	"ramdisk_size=21504" (ou, se isso também falhar, um valor mais alto)
	deve resolver isso. Até agora, esse problema foi relatado para
	HP9000-712/60 e HP715/100XC.
	</li>

	<li><b>O Debian suporta dispositivos com número limitado de blocos por
	padrão.</b>
	Se você tiver vários controladores de disco rígido, talvez seja
	necessário criar nós de dispositivo adicionais antes de reiniciar.
	Atualmente o Debian suporta apenas /dev/hd[a-h][1-20] e
	/dev/sd[a-h][1-15] por padrão. Isso não é um problema durante a
	instalação propriamente dita, porque o instalador usa um /dev dinâmico,
	mas pode levar a falhas durante a reinicialização após a instalação.
	</li>
</ul>

<p>
Versões melhoradas do sistema de instalação estão sendo desenvolvidas para a
próxima versão do Debian e também podem ser usadas para instalar o sarge.
Para detalhes, veja a página do
<a href="$(HOME)/devel/debian-installer/">projeto Debian-Installer</a>.
</p>

<h1 id="speakup">Instalador para cegos - "speakup"</h1>
<p>
Para a arquitetura i386, o Debian fornece um conjunto especial de
<a href="http://ftp.debian.org/debian/dists/sarge/main/installer-i386/current/images/floppy/access/">imagens
de instalação</a> para disquetes que suportam linhas braille. Uma lista
completa dos modelos suportados pode ser encontrada na
<a href="http://mielke.cc/brltty/details.html#displays">documentação brltty</a>.
</p>
