#use wml::debian::template title="Errata: executando o Linux 2.2.x no slink"
#use wml::debian::translation-check translation="8da95139c3595d47371ba8d288784086ae2ebacd"

<p>

Esta página documenta problemas conhecidos com a execução do kernel
Linux 2.2.x no Debian 2.1 (slink). Ele pressupõe que você esteja executando u
 sistema slink totalmente atualizado.

<p>

A versão slink é certificada e testada oficialmente para uso com os últimos
kernels do Linux 2.0.x. Como o congelamento do Debian ocorreu antes do
lançamento do Linux 2.2, e a alteração do número principal da versão do kernel
pode introduzir problemas complexos, foi decidido manter a já testada linha de
kernels 2.0.

<p>

As versões Debian, no entanto, não são necessariamente vinculadas à versão do
kernel.
Você pode executar qualquer kernel que desejar no Debian. Porém, simplesmente
não podemos garantir que tudo funcionará corretamente. Se você decidir
avançar para a linha Linux 2.2 e tiver problemas com um pacote, poderá ter
sorte executando a versão <a href="../potato/">potato</a> (Debian 2.2) desse
pacote.

<p>

Existem muitos links abaixo apontando para versões potato de pacotes.
Observe que se você instalar esses pacotes em uma máquina estável (stable),
poderá ser necessário instalar também bibliotecas do potato ou outras
dependências de pacotes.
Notadamente, você provavelmente terá que atualizar seu pacote libc6. Sugerimos
o uso do <code>apt-get</code> para esse fim, que obterá apenas os pacotes
necessários quando usado corretamente. No entanto, esteja avisado(a):
enquanto a maioria dos(as) usuários(as) não tem problemas para executar um
sistema misto estável (stable)/congelado (frozen), você pode ser incomodado(a)
por erros transitórios no potato.

<p>

O pacote <a href="https://packages.debian.org/kernel-source-2.2.1">kernel-source-2.2.1</a>
é entregue na distribuição para ajudar os(as) usuários(as) que desejam executar
os Kernels do Linux 2.2.x. No entanto, é recomendável verificar os sites da
distribuição do kernel padrão como o
<a href="https://www.kernel.org/">kernel.org</a> para obter versões mais
recentes da árvore do código-fonte 2.2.x e erratas adicionais. Existem erros
conhecidos no 2.2.1, e sabe-se que essa versão causou corrupção de dados para
algumas pessoas. Você deve obter os patches para o kernel da série 2.2 mais
recente e aplicá-los à árvore de fontes do kernel do Linux.

<h2>Pacotes potencialmente problemáticos</h2>

<p>

Observe que esta lista pode estar incompleta. Por favor envie um bug contra
www.debian.org se você encontrar outros problemas não listados. Verifique
também os logs de bugs para o pacote em questão; tente garantir que o problema
foi introduzido no Linux 2.2.

<dl>
	<dt><a href="https://packages.debian.org/sysutils">sysutils</a>
	<dd>
<tt>procinfo</tt> não será executado. A versão do
<a href="https://www.debian.org/Packages/frozen/utils/sysutils.html">
potato</a> corrige isso.
	</dd>
	
	<dt><a href="https://packages.debian.org/netbase">netbase</a>
	<dd>
No Linux 2.2, o <tt>ipautofw</tt> deve ser substituído pelo  <tt>ipmasqadm</tt>
e o <tt>ipfwadm</tt> é substituído com <tt>ipchains</tt>.  O pacote do potato
<a href="https://www.debian.org/Packages/frozen/base/netbase.html">
netbase</a> contém um script wrapper,
<tt>ipfwadm-wrapper</tt>, para facilitar a transição.
<p>
<tt>ifconfig</tt> não mostrará interfaces de alias e, em algumas
circunstâncias, <tt>ipchains</tt> falhará silenciosamente ao limpar contadores
de pacotes.  Algumas rotas criadas pelos scripts init <tt>netbase</tt> causarão
mensagens de aviso inofensivas.
<p>
Todos esses problemas são resolvidos na versão
<a href="https://www.debian.org/Packages/frozen/base/netbase.html">
potato</a>. Se você não deseja atualizar para o potato, pacotes compatíveis com
o Debian 2.1 ficaram
<a href="https://www.debian.org/~rcw/2.2/netbase/">disponíveis</a>.
	</dd>
	
	<dt><a href="https://packages.debian.org/pcmcia-source">pcmcia-source</a>
	<dd>
A versão do <tt>pcmcia-source</tt> no slink não pode ser compilado com o kernel
2.2.  Corrigido na versão
<a href="https://www.debian.org/Packages/frozen/admin/pcmcia-source.html">
potato</a>.
	</dd>
	
	<dt><a href="https://packages.debian.org/dhcpcd">dhcpcd</a>
	<dd>
Quebra no Linux 2.2; use a versão
<a href="https://www.debian.org/Packages/frozen/net/dhcpcd.html">potato</a>.
	</dd>
	
	<dt><a href="https://packages.debian.org/dhcp-client-beta">dhcp-client-beta</a>
	<dd>
O <tt>/etc/dhclient-script</tt> não funciona com o 2.2. A versão do
<a href="https://www.debian.org/Packages/frozen/net/dhcp-client.html">
potato</a> corrige isso; observe que o pacote foi renomeado para apenas
<code>dhcp-client</code>.
	</dd>
	
	<dt><a href="https://packages.debian.org/wanpipe">wanpipe</a>
	<dd>
A versão 2.0.1 no slink é incompatível com os kernels 2.2.
A versão 2.0.4 e posterior que você pode pegar do
<a href="https://www.debian.org/Packages/frozen/net/wanpipe.html">potato</a>,
funcionará para os kernels 2.2, mas não com os kernels 2.0 (contudo, um patch
para o kernel 2.0 está incluso na versão do potato).
	</dd>
	
	<dt><a href="https://packages.debian.org/netstd">netstd</a>
	<dd>
O <tt>bootpc</tt> não receberá uma resposta, a menos que a interface já esteja
configurada. Isso foi corrigido no agora separado
<a href="https://packages.debian.org/bootpc">pacote bootpc</a> no potato.
	</dd>
	
	<dt><a href="https://packages.debian.org/lsof">lsof</a>
	<dd>
O <tt>lsof</tt> precisa ser recompilado para funcionar com o linux 2.2. Atualize
o pacote <tt>lsof</tt> disponível no potato.
	</dd>
	
	<dt><a href="https://packages.debian.org/acct">acct</a>
	<dd>
A estrutura de contagem mudou no kernel 2.2, portanto, se você estiver
executando o <tt>acct</tt> e o Linux 2.2, precisará da versão do pacote do
potato (que é incompatível com os kernels da série 2.0).
	</dd>
	
	<dt><a href="https://packages.debian.org/isdnutils">isdnutils</a>
	<dd>
O <tt>isdnutils</tt> 3.0 ou superior no Debian deve funcionar com os kernels das
séries 2.0 e 2.2. Isso acontece porque o(a) mantenedor(a) do pacote
Debian se envolveu com problemas especiais para garantir que isso acontecesse.
Outras distribuições podem não ter tanta sorte.
	</dd>
	
	<dt><a href="https://packages.debian.org/diald">diald</a>
	<dd>
O pacote <tt>diald</tt> no slink tem problemas ao criar rotas dinamicamente no
Linux 2.2. Atualize para a versão disponível no potato.
	</dd>
	
        <dt><a href="https://packages.debian.org/xosview">xosview</a>
	<dd>
O <tt>xosview</tt> fará um loop infinito no Linux 2.2.2 e superior.
Atualize para a versão disponível no potato.
	</dd>

</dl>


<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-indent-data:nil
sgml-doctype:"../.doctype"
End:
-->
