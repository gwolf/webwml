#use wml::debian::template title="Envio de informações sobre um espelho"
#use wml::debian::translation-check translation="cd8f39027a950a5174d2d49109ba6800c0f0cdec" translation_maintainer="Tassia Camoes Araujo"
#include "$(ENGLISHDIR)/mirror/submit.inc"

<p>Se você deseja enviar informações sobre um espelho do Debian, utilize o
o formulário abaixo. As informações fornecidas serão exibidas na lista de
espelhos.</p>

<p>Por favor, observe que todos os espelhos enviados devem seguir as
<a href="ftpmirror">regras e definições presentes em nossa documentação</a>. Em
particular,
</p>
<ul>
<li>você deve usar <a href="ftpmirror#how">ftpsync</a> para espelhar o Debian,</li>
<li>escolha um boa fonte (<strong>não</strong> um serviço (http) com nome, por
    exemplo, <code>ftp.CC.debian.org</code>, nem DNS round robins, nem CDNs),</li>
<li>atualize quatro vezes por dia para corresponder à frequência de atualização
    do repositório (ou configure gatilhos na fonte, ou use
    <code>ftpsync-cron</code> uma vez a cada hora para monitorar sua fonte por
    mudanças e iniciar o processo de sincronização). Além disso,</li>
<li>seu espelho deve ter um arquivo tracefile nomeado apropriadamente (ftpsync
    vai resolver isso se MIRRORNAME estiver definido corretamente).</li>
<li>você deve criar os arquivos de sinalização
    <code>/Archive-Update-in-Progress-XXX</code> e
    <code>/Archive-Update-Required-XXX</code> quando apropriado
    (novamente, ftpsync fará isso para você) para ajudar espelhos que usem o seu
    como fonte a espelharem corretamente.</li>
</ul>

<form-action "" archive-upstream https://cgi.debian.org/cgi-bin/submit_mirror.pl>

<h2>Informações básicas</h2>

<p>
<input type="radio" name="submissiontype" value="new" checked>
Cadastro de novo espelho
&nbsp; &nbsp; &nbsp;
<input type="radio" name="submissiontype" value="update">
Atualização de um espelho existente
</p>

<p>
Nome do espelho: <input type="text" name="site" size="30"></p>

<p>Insira nos campos abaixo o endereço do espelho Debian em seu site.
Deixe campos irrelevantes em branco.</p>

<table>
  <tr><td>Repositório de pacotes, sobre HTTP: </td><td><input type="text" name="archive-http" id="archve-http" size="30" value="/debian" readonly="readonly"> <small>O repositório deve estar disponível em <code>/debian</code>.</small></td></tr>
<tablerowdef "Repositório de pacotes, sobre rsync"  archive-rsync  30 "debian" " <small>Se você oferecer rsync, <code>debian</code> é o termo sugerido como nome de módulo.</small>">
# <tablerow "imagens CD/DVD, sobre HTTP"      cdimage-http   30>
# <tablerow "imagens CD/DVD, sobre rsync"     cdimage-rsync  30>
# <tablerow "Lançamentos antigos do Debian, sobre HTTP"  old-http     30>
# <tablerow "Lançamentos antigos do Debian, sobre rsync" old-rsync    30>
</table>

<h2>Informações sobre o site do espelho</h2>

<table>
<tr>
<td>Arquiteturas espelhadas:
<td>
<label><input type=checkbox name=architectures id="allarch" value="ALL" onclick="allarches()">&nbsp;<em>todas as arquiteturas (ou seja, nenhuma exclusão por arquitetura é feita)</em></label><br>
<archlist>
</td></tr>
</table>

<table>
<tablerow "Nome do(a) mantenedor(a) do site"         maint_name    30>
<tablerow "E-mail público do(a) mantenedor(a) do site"              maint_public_email    30>
<tr><td>País do site:  <td><select name="country">
<countrylist>
</select>
<tablerow "Localização do site (opcional)"     location      30>
<tablerow "Nome do patrocinador do site (opcional)" sponsor_name  30>
<tablerow "URL do patrocinador do site (opcional)"  sponsor_url   30>
</table>

<table><tr>
<td valign="top">Comentário:</td>
<td><textarea name="comment" cols=40 rows=7></textarea></td>
</tr></table>

<p><label>Eu me inscrevi na
<a href="https://lists.debian.org/debian-mirrors-announce/">
lista de e-mails para anúncios</a>
<input type="checkbox" name="mlannounce"></label>
</p>

<p>
<input type="submit" value="Submit"> <input type="reset" value="Clear form">
</p>
</form>

<p>Seu site deve aparecer na lista dentro de algumas semanas, assim que um
operador humano verificá-lo e incluí-lo. Nós enviaremos um e-mail para
você em caso de algum problema com as informações fornecidas.</p>

<p>Se você não receber nenhum retorno dentro de três meses, você pode
nos contatar em <email mirrors@debian.org>.</p>

