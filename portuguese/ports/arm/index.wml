#use wml::debian::template title="Porte ARM"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::toc
#use wml::debian::translation-check translation="51585e94038fbe80235329bd777a3ef8e5016e69" maintainer="Tiago Verçosa"

<toc-display/>

<toc-add-entry name="about">Debian no ARM</toc-add-entry> <p>Nestas páginas,
você vai encontrar informações sobre o esforço contínuo de portar o
Debian GNU/Linux para várias versões da <a
href="https://en.wikipedia.org/wiki/ARM_architecture">arquitetura ARM</a>,
que são encontradas em todos os tipos de sistemas, desde embarcados até grandes
servidores.</p>

<toc-add-entry name="status">Estado Atual</toc-add-entry>
<p>O Debian suporta completamente três portes para diferentes tipos de hardware
ARM little-endian:</p>

<ul>

<li>O porte <a href="https://wiki.debian.org/ArmEabiPort">ARM EABI</a>
(armel) tem como alvo uma variedade de dispositivos ARM de 32 bits mais antigos,
particularmente aqueles usados em hardware NAS e uma variedade de computadores
plug (sistemas embarcados).</li>

<li>O porte mais recente de <a href="https://wiki.debian.org/ArmHardFloatPort">
ARM hard-float</a> (armhf) suporta os novos e mais poderosos dispositivos de
32 bits usando a versão 7 da especificação de arquitetura ARM.</li>

<li>O porte <a href="https://wiki.debian.org/Arm64Port">ARM de 64 bits</a>
(arm64) suporta os mais recentes dispositivos ARM de 64-bits.</li>

</ul>

<p>Outros portes para hardware ARM existem / existiram no Debian e derivados
- veja <a href="https://wiki.debian.org/ArmPorts">a wiki</a> para mais links e
uma visão geral.</p>

<p>Para uma lista completa e atualizada dos diferentes hardwares suportados
por cada porte, consulte as respectivas páginas wiki. Novos dispositivos ARM
são lançados toda semana e é mais fácil para as pessoas manterem as informações
atualizadas lá.</p>

<toc-add-entry name="availablehw">Hardware disponível para desenvolvedores(as) Debian</toc-add-entry>
<p>Várias máquinas são disponibilizadas para os(as) desenvolvedores(as) Debian
trabalharem no porte para ARM: abel.debian.org (armel/armhf), asachi.debian.org
(armhf/arm64) e harris.debian.org (armhf). As máquinas possuem ambientes
de desenvolvimento chroot que você pode acessar com <em>schroot</em>. Por
favor, veja o <a
href="https://db.debian.org/machines.cgi">banco de dados de máquinas</a> para
mais informações sobre essas máquinas.</p>

<toc-add-entry name="contacts">Contatos</toc-add-entry>
<h3>Listas de discussão</h3>

<p>A lista de discussão do porte ARM do Debian está localizada em
<email "debian-arm@lists.debian.org">.
Se você deseja se inscrever, envie uma mensagem com a palavra <q>subscribe</q>
como o assunto para <email "debian-arm-request@lists.debian.org">. A lista está
arquivada nos <a href="https://lists.debian.org/debian-arm/">arquivos da lista
debian-arm</a>.</p>

<p>
Também é uma boa ideia assinar a lista de discussão
<a href="http://www.arm.linux.org.uk/mailinglists/">\
linux-arm</a>.</p>

<h3>IRC</h3>

<p>Você pode nos encontrar no IRC em <em>irc.debian.org</em> no canal
<em>#debian-arm</em>.</p>

<toc-add-entry name="people">Pessoas</toc-add-entry>
<p>
Esta é uma lista de pessoas importantes que estão atualmente envolvidas no
porte ARM do Debian.
</p>

<ul>

<li>Ian Campbell <email "ijc@debian.org">
<br />
debian-installer, kernel
</li>

<li>Aurelien Jarno <email "aurel32@debian.org">
<br />
Mantenedor do buildd ARM e portes em geral
</li>

<li>Steve McIntyre <email "steve@einval.com">
<br />
Administrador local para máquinas ARM, documentação e portes em geral.
</li>

<li>Martin Michlmayr <email "tbm@cyrius.com">
<br />
Documentação, debian-installer
</li>

<li>Riku Voipio <email "riku.voipio@iki.fi">
<br />
porte armel e mantenedor do buildd
</li>

<li>Wookey <email "wookey@aleph1.co.uk">
<br />
Documentação
</li>

</ul>

<toc-add-entry name="dedication">Dedicatória</toc-add-entry>

<p>Chris Rutter,
que foi o coordenador do projeto e do autobuilder para o porte Debian ARM,
morreu em um acidente de carro. Nós dedicamos a versão do porte ARM na
distribuição Debian GNU/Linux <q>woody</q> em sua memória.</p>

<toc-add-entry name="thanks">Agradecimentos</toc-add-entry>

<p>
Estas pessoas ajudaram a tornar o porte ARM viável para o Debian:

Jim Studt, Jim Pick, Scott Bambrough, Peter Naulls, Tor Slettnes,
Phil Blundell, Vincent Sanders
</p>
