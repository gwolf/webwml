#use wml::debian::template title="Introduction to Debian" MAINPAGE="true" 
#use wml::debian::recent_list
#use wml::debian::translation-check translation="93f9b3008408920efa7a3791fb329de4c58aac6c" maintainer="galaxico"

<a id=community></a>
<h2>Το Debian είναι μια Κοινότητα ατόμων</h2>
<p>Χιλιάδες εθελοντές σε ολόκληρο τον κόσμο δουλεύουν από κοινού βάζοντας ως προτεραιότητα το Ελεύθερο Λογισμικό και τις ανάγκες των χρηστών.</p>

<ul>
  <li>
    <a href="people">Άνθρωποι:</a>
    Ποιοι είμαστε, τι κάνουμε
  </li>
  <li>
    <a href="philosophy">Φιλοσοφία:</a>
    Γιατί το κάνουμε και γιατί το κάνουμε
  </li>
  <li>
    <a href="../devel/join/">Εμπλακείτε:</a>
    Μπορείτε να πάρετε μέρος σ' αυτό!
  </li>
  <li>
    <a href="help">Πώς μπορείτε να βοηθήσετε το Debian;</a>
  </li>
  <li>
    <a href="../social_contract">Κοινωνικό Συμβόλαιο:</a>
    Η ηθική μας ατζέντα
  </li>
  <li>
    <a href="diversity">Δήλωση για τη Διαφορετικότητα</a>
  </li>
  <li>
    <a href="../code_of_conduct">Κώδικας Συμπεριφοράς</a>
  </li>
  <li>
    <a href="../partners/">Συνεργάτες:</a>
    Εταιρείες και οργανισμοί που προσφέρουν σταθερή βοήθεια στο Σχέδιο του Debian
  </li>
  <li>
    <a href="../donations">Δωρεές</a>
  </li>
  <li>
    <a href="../legal/">Νομικές πληροφορίες</a>
  </li>
  <li>
    <a href="../legal/privacy">Ιδιωτικότητα Δεδομένων</a>
  </li>
  <li>
    <a href="../contact">Επικοινωνήστε μαζί μας</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h2>Το Debian είναι ένα Ελεύθερο Λειτουργικό Σύστημα</h2>
<p>Ξεκινούμε με το Linux και προσθέτουμε χιλιάδες ακόμα εφαρμογές ώστε να ικανοποιήσουμε τις ανάγκες των χρηστών του.</p>

<ul>
  <li>
    <a href="../distrib">Μεταφόρτωση:</a>
    Περισσότερες εκδοχές εικόνων του Debian
  </li>
  <li>
  <a href="why_debian">Γιατί το Debian</a>
  </li>
  <li>
    <a href="../support">Υποστήριξη:</a>
    Αποκτώντας βοήθεια:
  </li>
  <li>
    <a href="../security">Ασφάλεια:</a>
    Last update <br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages"> Πακέτα λογισμικού:</a>
    Αναζητήστε και φυλλομετρήστε τον μακρύ κατάλογο του λογισμικού μας
  </li>
  <li>
    <a href="../doc"> Τεκμηρίωση</a>
  </li>
  <li>
    <a href="https://wiki.debian.org"> Το Wiki του Debian</a>
  </li>
  <li>
    <a href="../Bugs"> Αναφορές σφαλμάτων</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">
    Λίστες Αλληλογραφίας</a>
  </li>
  <li>
    <a href="../blends"> Καθαρά Μείγματα:</a>
    Μεταπακέτα για συγκεκριμένες ανάγκες
  </li>
  <li>
    <a href="../devel"> Η Γωνιά των Προγραμματιστ(ρι)ών:</a>
    Πληροφορίες που ενδιαφέρουν κυρίως για τους/τις προγραμματιστές/προγραμματίστριες του Debian
  </li>
  <li>
    <a href="../ports"> Υλοποιήσεις/Αρχιτεκτονικές:</a>
    Αρχιτεκτονικές CPU που υποστηρίζουμε
  </li>
  <li>
    <a href="search">Πληροφορίες για την χρήση της μηχανής αναζήτησης του Debian</a>.
  </li>
  <li>
    <a href="cn">Πληροφορίες σχετικά με σελίδες που είναι διαθέσιμες σε πολλαπλές γλώσσες </a>.
  </li>
</ul>
