#use wml::debian::template title="Εκδόσεις του Debian"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="ff05f695462dd08fe524856384ed03a2dbb6763f" maintainer="galaxico"

<p>Το Debian έχει πάντα τουλάχιστον τρεις εκδόσεις σε ενεργή συντήρηση: 
<q>Σταθερή (stable)</q>, <q>Δοκιμαστική (testing)</q> και
<q>Ασταθή (unstable)</q>.</p>

<dl>
<dt><a href="stable/">Σταθερή (stable)</a></dt>
<dd>
<p>
  Η <q>σταθερή</q> διανομή περιέχει την επίσημη διανομή του Debian που 
  έχει κυκλοφορήσει πιο πρόσφατα.
</p>
<p>
  Είναι η παραγωγική διανομή του Debian, αυτή την οποία πρωτίστως συνιστούμε 
για χρήση.
</p>
<p>
  Η τρέχουσα <q>σταθερή</q> διανομή του Debian είναι η έκδοση 
  <:=substr '<current_initial_release>', 0, 2:>, με κωδική ονομασία 
<em><current_release_name></em>.
<ifeq "<current_initial_release>" "<current_release>"
  "Κυκλοφόρησε στις <current_release_date>."
/>
<ifneq "<current_initial_release>" "<current_release>"
  "Κυκλοφόρησε αρχικά ως έκδοση
<current_initial_release>
  στις <current_initial_release_date> και η πιο πρόσφατη επικαιροποίησή της, 
η έκδοση  <current_release>, Κυκλοφόρησε στις
<current_release_date>."
/>
</p>
</dd>

<dt><a href="testing/">Δοκιμαστική (testing)</a></dt>
<dd>
<p>
  Η <q>δοκιμαστική</q> διανομή περιέχει πακέτα που δεν έχουν γίνει ακόμα δεκτά 
στην <q>σταθερή</q> έκδοση, αλλά είναι στην "ουρά" γι' αυτό. Το κύριο 
πλεονέκτημα της χρήσης αυτής της διανομής είναι ότι διαθέτει πιο πρόσφατες 
εκδόσεις λογισμικού.
</p>
<p>
  Δείτε τις συχνές ερωτήσεις στο <a href="$(DOC)/manuals/debian-faq/">Debian 
FAQ</a> για περισσότερες πληροφορίες σχετικά με το 
  <a href="$(DOC)/manuals/debian-faq/ch-ftparchives#s-testing">τι είναι 
η<q>δοκιμαστική</q></a> διανομή
  και <a href="$(DOC)/manuals/debian-faq/ch-ftparchives#s-frozen">πώς γίνεται
  <q>σταθερή</q></a>.
</p>
<p>
  Η τρέχουσα <q>δοκιμαστική</q> διανομή είναι η <em><current_testing_name></em>.
</p>
</dd>

<dt><a href="unstable/">Ασταθής (unstable)</a></dt>
<dd>
<p>
  H <q>ασταθής</q> είναι η διανομή στην οποία γίνεται η πραγματική ανάπτυξη του  
 Debian.  Γενικά, αυτή η διανομή χρησιμοποιείται από προγραμματιστές και 
εκείνους που τους αρέσει να ζουν στην "αιχμή". Συνιστάται ότι οι χρήστες που 
χρησιμοποιούν την ασταθή διανομή να γράφονται στην λίστα 
αλληλογραφίας debian-devel-announce για να λαμβάνουν τις ενημερώσεις για 
σημαντικές αλλαγές, για παράδειγμα προβληματικές.
</p>
<p>
  Η <q>ασταθής</q> διανομή λέγεται πάντα <em>sid</em>.
</p>
</dd>
</dl>

<h2>Ο κύκλος ζωής μιας έκδοσης</h2>
<p>
  Το Debian αναγγέλει την καινούρια σταθερή έκδοσή της σε μια τακτική βάση.  
 Οι χρήστες μπορούν να περιμένουν 3 χρόνια πλήρους υποστήριξης για κάθε 
έκδοση 2 επιπλέον χρόνα υποστήριξης Μακράς Διάρκειας (LTS, Long Term Support).
</p>

<p>
  Δείτε τις σελίδες <a 
href="https://wiki.debian.org/DebianReleases">Εκδόσεις του Debian 
</a> και <a href="https://wiki.debian.org/LTS">Debian LTS</a>
  στο Wiki για αναλυτικές πληροφορίες.
</p>

<h2>Κατάλογος εκδόσεων</h2>

<ul>

  <li><a href="<current_testing_name>/">Η επόμενη έκδοση του Debian 
έχει την κωδική ονομασία 
    <q><current_testing_name></q></a>
       &mdash; δεν έχει ακόμα οριστεί ημερομηνία κυκλοφορίας
      </li>

  <li><a href="buster/">Debian 10 (<q>buster</q>)</a>
      &mdash; η τρέχουσα σταθερή έκδοση
  </li>

  <li><a href="stretch/">Debian 9 (<q>stretch</q>)</a>
      &mdash; η παλιά (oldstable) σταθερή έκδοση
  </li>

  <li><a href="jessie/">Debian 8 (<q>jessie</q>)</a>
      &mdash; η oldoldstable έκδοση
  </li>
  
  <li><a href="wheezy/">Debian 7 (<q>wheezy</q>)</a>
      &mdash; παρωχημένη σταθερή έκδοση
  </li>

  <li><a href="squeeze/">Debian 6.0 (<q>squeeze</q>)</a>
      &mdash; παρωχημένη σταθερή έκδοση
  </li>

  <li><a href="lenny/">Debian GNU/Linux 5.0 (<q>lenny</q>)</a>
      &mdash; παρωχημένη σταθερή έκδοση
  </li>

  <li><a href="etch/">Debian GNU/Linux 4.0 (<q>etch</q>)</a>
      &mdash; παρωχημένη σταθερή έκδοση
  </li>
  <li><a href="sarge/">Debian GNU/Linux 3.1 (<q>sarge</q>)</a>
      &mdash; παρωχημένη σταθερή έκδοση
  </li>
  <li><a href="woody/">Debian GNU/Linux 3.0 (<q>woody</q>)</a>
      &mdash; παρωχημένη σταθερή έκδοση
  </li>
  <li><a href="potato/">Debian GNU/Linux 2.2 (<q>potato</q>)</a>
      &mdash; παρωχημένη σταθερή έκδοση
  </li>
  <li><a href="slink/">Debian GNU/Linux 2.1 (<q>slink</q>)</a> 
      &mdash; παρωχημένη σταθερή έκδοση
  </li>
  <li><a href="hamm/">Debian GNU/Linux 2.0 (<q>hamm</q>)</a>
      &mdash; παρωχημένη σταθερή έκδοση
  </li>
</ul>

<p>Οι ιστοσελίδες για τις παρωχημένες εκδόσεις του Debian διατηρούνται άθικτες, 
αλλά οι ίδιες οι διανομές μπορούν να βρεθούν μόνο σε μια ξεχωριστή αρχειοθήκη
<a href="$(HOME)/distrib/archive">archive</a>.</p>

<p>Δείτε τις συχνές ερωτήσεις στο <a 
href="$(HOME)/doc/manuals/debian-faq/">Debian FAQ</a> για την εξήγηση της
<a href="$(HOME)/doc/manuals/debian-faq/ch-ftparchives#s-sourceforcodenames">
 προέλευσης όλων αυτών των κωδικών ονομασιών</a>.</p>

<h2>Ακεραιότητα των δεδομένων στις εκδόσεις</h2>

<p>Η ακεραιότητα των δεδομένων εξασφαλίζεται από ένα ψηφιακά υπογεγραμμένο 
αρχείο <code>Release</code>. Για να διασφαλιστεί ότι όλα τα αρχεία στην έκδοση 
ανήκουν σ' αυτήν, τα checksum όλων των αρχείων των <code>Πακέτων</code> 
αντιγράφονται στο αρχείο <code>Release</code>.</p>

<p>Ψηφιακές υπογραφές για το αρχείο αυτό αποθηκεύονται στο αρχείο
<code>Release.gpg</code>, χρησιμοποιώντας την τρέχουσα έκδοση του κλεδιού 
υπογραφής της αρχειοθήκης. Για τις εκδόσεις <q>stable</q> και <q>oldstable</q> 
παράγεται μια επιπλέον υπογραφή χρησιμοποιώντας ένα offline κλειδί που 
παράγεται ειδικά για την έκδοση από ένα μέλος της <a 
href="$(HOME)/intro/organization#release-team">\
Stable Release Team (Ομάδας κυκλοφορίας της σταθερής έκδοσης)</a>.</p>
