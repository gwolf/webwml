msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2011-01-10 22:47+0100\n"
"Last-Translator: Szabolcs Siebenhofer\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Első lépés"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Második lépés"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Harmadik lépés"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Negyedik lépés"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Ötödik lépés"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Hatodik lépés"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Hetedik lépés"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Jelöltek ellenőrzési listája"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Új Tagok Sarka"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"További információk: <a href=\"m4_HOME/intl/french/\">https://www.debian.org/"
"intl/french/</a> (csak franciául elérhető)"

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "További tudnivalók"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"További információk:<a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/"
"intl/spanish/</a> (csak spanyolul elérhető)"

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Telefon"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Fax"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Cím"

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr "Nemzetközi szállítás:"

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr "Európán belül"

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr "Származási ország:"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "Termékek"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "T Pólók"

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr "Debian-nak adományozott pénz"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "sapkák"

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr "Szabad szoftver események szervezésére felhasznált pénz"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "maricák"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "bögrék"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "más öltözékek"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "Pólók"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "frisbee-k"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "egérpadok"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "jelvények"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "kosárlabda gólok"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "fülbevalók"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "utitáskák"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "esernyők"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr "párnahuzat"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr "kulcstartók"

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr "Svájci bicskák"

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "USB kulcsok"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr "rögzítők"

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr "mások"

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr "Elérhető nyelvek:"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "Beágyazott PostScript"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Powered by Debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Powered by Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Debian powered]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (mini gomb)"

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr ",,Debian''-nal"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr ",,Debian'' nélkül"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "mint fent"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "Mióta használsz Debian-t?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Debian fejlesző vagy?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "A Debian melyik részével foglalkozol?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "Miért kezdél a Debian-nal foglalkozni?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""
"Van ötleted olyan hölgyeknek, akik mélyebben szeretnek Debian-nal "
"foglalkozni?"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr "Része vagy más női technológiai csoportnak? Melyik(ek)nek?"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Picit több rólad..."

#~ msgid "Working"
#~ msgstr "Munka"

#~ msgid "sarge"
#~ msgstr "sarge"

#~ msgid "sarge (broken)"
#~ msgstr "sarge (hibás)"

#~ msgid "Booting"
#~ msgstr "Betöltés"

#~ msgid "Building"
#~ msgstr "Felépítés"

#~ msgid "Not yet"
#~ msgstr "Még nem"

#~ msgid "No kernel"
#~ msgstr "Nincs kernel"

#~ msgid "No images"
#~ msgstr "Nincsenek képek"

#~ msgid "<void id=\"d-i\" />Unknown"
#~ msgstr "Ismeretlen"

#~ msgid "Unavailable"
#~ msgstr "Nem elérhető"

#~ msgid "Download"
#~ msgstr "Letöltés"

#~ msgid "Old banner ads"
#~ msgstr "Régebbi bannerreklámok"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "BAD"
#~ msgstr "ROSSZ"

#~ msgid "OK?"
#~ msgstr "Rendben?"

#~ msgid "BAD?"
#~ msgstr "ROSSZ?"

#~ msgid "??"
#~ msgstr "??"

#~ msgid "Unknown"
#~ msgstr "Ismeretlen"

#~ msgid "ALL"
#~ msgstr "MINDEN"

#~ msgid "Package"
#~ msgstr "Csomag"

#~ msgid "Status"
#~ msgstr "Állapot"

#~ msgid "Version"
#~ msgstr "Verzió"

#~ msgid "URL"
#~ msgstr "URL"

#~ msgid "Wanted:"
#~ msgstr "Keressük"

#~ msgid "Who:"
#~ msgstr "Ki:"

#~ msgid "Architecture:"
#~ msgstr "Architektúra:"

#~ msgid "Specifications:"
#~ msgstr "Specifikációk:"

#~ msgid "Where:"
#~ msgstr "Hol:"

#~ msgid "Older Debian banners"
#~ msgstr "Régebbi Debian-bannerek"

#~ msgid "animated&nbsp;GIF&nbsp;banner"
#~ msgstr "mozgó GIF-banner"

#~ msgid "horizontal&nbsp;banner"
#~ msgstr "vízszintes banner"

#~ msgid "vertical&nbsp;banner"
#~ msgstr "függőleges banner"

#~ msgid "Q"
#~ msgstr "K"

#~ msgid ""
#~ "<total_consultant> Debian consultants listed in <total_country> countries "
#~ "worldwide."
#~ msgstr ""
#~ "A világ <total_country> országának <total_consultant> Debian-tanácsadója"

#~ msgid "Willing to Relocate"
#~ msgstr "Bárhol hajlandó"

#~ msgid "Rates:"
#~ msgstr "Árak:"

#~ msgid "Email:"
#~ msgstr "E-mail:"

#~ msgid "or"
#~ msgstr "vagy"

#~ msgid "URL:"
#~ msgstr "URL:"

#~ msgid "Company:"
#~ msgstr "Cég:"

#~ msgid "Name:"
#~ msgstr "Név:"
