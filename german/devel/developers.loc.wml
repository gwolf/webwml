#use wml::debian::template title="Weltkarte der Entwickler"
#use wml::debian::translation-check translation="6c996020eca890cac2a0243d4633d9bfa7935674"
# $Id$
# Translator: Alexander Reiter <leckse@tm1.at>

<p>Viele haben Interesse an den Standorten der Debian-Entwickler
bekundet. Wir haben uns deshalb entschlossen, in der Entwicklerdatenbank
ein Feld hinzuzufügen, in welches die Entwickler ihre Koordinaten
eingeben können.

<p>Die untenstehende Karte wurde mit einer anonymisierten
<a href="developers.coords">Liste der Entwicklerkoordinaten</a>
unter Zuhilfenahme von <a href="https://packages.debian.org/stable/graphics/xplanet">xplanet</a> erzeugt.

<p><img src="developers.map.jpeg" alt="Weltkarte">

<p>Wenn Sie Entwickler sind und Ihre Koordinaten der Datenbank
hinzufügen möchten, loggen Sie sich in die
<a href="https://db.debian.org">Entwicklerdatenbank</a> ein
und bearbeiten Sie dort Ihren Eintrag. Wenn Sie die Koordinaten von
Ihrem Heimatort nicht wissen, können Sie sie unter
folgender Adresse herausfinden:</p>

<ul>
<li><a href="https://osm.org">Openstreetmap</a>
Sie können über die Suchleiste nach Ihrer Stadt suchen.
Wählen Sie dann die Richtungspfeile neben der Suchleiste (Routenberechnung).
Ziehen Sie jetzt den grünen Marker auf die OSM-Karte. Die Koordinaten
erscheinen in der »From«-Zeile.
</ul>

<p>Das Format für die Koordinaten sollte wie eines der folgenden aussehen:
<dl>
<dt>Grade
<dd>Das Format sollte so aussehen: +-GGG.GGGGGGGGGGGGGGG. Dieses Format
wird von Programmen wie xearth und vielen Websites benutzt. Jedoch ist
die Genauigkeit auf 4 bis 5 Nachkommastellen beschränkt.
<dt>Grade Minuten (DGM)
<dd>Das Format sieht so aus: +-GGGMM.MMMMMMMMMMMMM. Das ist keine
arithmetische Art, sondern eine komprimierte Darstellung von zwei
verschiedenen
Einheiten: Grad und Minuten. Einige tragbare GPS- und NMEA-Geräte geben
so etwas üblicherweise aus.
<dt>Grade Minuten Sekunden (DGMS)
<dd>+-GGGMMSS.SSSSSSSSSSS. Wie auch DGM ist das kein arithmetischer Typ
sondern eine komprimierte Darstellung von drei verschiedenen Einheiten:
Grade, Minuten und Sekunden.
Das Format entwickelte sich aus Websites, die 3 verschiedene Werte für
jede Position ausgaben. Zum Beispiel sei 34:50:12.24523 Nord die gegebene
Position, dann wäre das +0345012.24523 in DGMS.
</dl>
<P>Für die Breitengrade ist der Norden positiv, und für die Längengrade
der Osten positiv. Es ist wichtig, genügend anführende Nullen anzugeben,
damit das Format eindeutig bleibt, falls Ihre Position weniger als 2
Grad vom Nullpunkt abweicht.
