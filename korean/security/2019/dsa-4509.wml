#use wml::debian::translation-check translation="b917e690cbacd447496fcc36bb3b22df5d6873b2" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
<define-tag description>보안 업데이트</define-tag>
<define-tag moreinfo>
<p>Apache HTTPD 서버에서 여러 취약점을 발견했습니다.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9517">CVE-2019-9517</a>

    <p>Jonathan Looney reported that a malicious client could perform a
    denial of service attack (exhausting h2 workers) by flooding a
    connection with requests and basically never reading responses on
    the TCP connection.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10081">CVE-2019-10081</a>

    <p>Craig Young reported that HTTP/2 PUSHes could lead to an overwrite
    of memory in the pushing request's pool, leading to crashes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10082">CVE-2019-10082</a>

    <p>Craig Young reported that the HTTP/2 session handling could be made
    to read memory after being freed, during connection shutdown.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10092">CVE-2019-10092</a>

    <p>Matei <q>Mal</q> Badanoiu reported a limited cross-site scripting
    vulnerability in the mod_proxy error page.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10097">CVE-2019-10097</a>

    <p>Daniel McCarney reported that when mod_remoteip was configured to
    use a trusted intermediary proxy server using the <q>PROXY</q> protocol,
    a specially crafted PROXY header could trigger a stack buffer
    overflow or NULL pointer deference. This vulnerability could only be
    triggered by a trusted proxy and not by untrusted HTTP clients. The
    issue does not affect the stretch release.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10098">CVE-2019-10098</a>

    <p>Yukitsugu Sasaki reported a potential open redirect vulnerability in
    the mod_rewrite module.</p></li>

</ul>

<p>옛안정 배포(stretch)에서, 이 문제를 버전 2.4.25-3+deb9u8에서 수정했습니다.</p>

<p>안정 배포(buster)에서, 이 문제를 버전 2.4.38-3+deb10u1에서 수정했습니다.</p>

<p>apache2 패키지를 업그레이드 하는 게 좋습니다.</p>

<p>apache2의 자세한 보안 상태는 보안 추적 페이지를 참조하십시오:
<a href="https://security-tracker.debian.org/tracker/apache2">https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4509.data"
