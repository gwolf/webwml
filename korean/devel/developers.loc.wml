#use wml::debian::template title="개발자 위치"
#use wml::debian::translation-check translation="6c996020eca890cac2a0243d4633d9bfa7935674" maintainer="Sebul" mindelta="-1"

<p>많은 사람들이 데비안 개발자들의 위치에 대한 정보에 관심을 보였습니다.
따라서 개발자가 자신의 세계 좌표를 지정할 수 있는 필드를 개발자 데이터베이스의 일부로 추가하기로 결정했습니다.
<p>아래 지도는 <a href="https://packages.debian.org/stable/graphics/xplanet">
xplanet</a>을 사용한 익명화된 <a href="developers.coords">개발자 좌표 목록</a>에서 만들었습니다. 

<p><img src="developers.map.jpeg" alt="World Map">

<p>If you are a developer and would like to add your coordinates
to your database entry, log in to the
<a href="https://db.debian.org">데비안 개발자 데이터베이스</a>
and modify your entry.
고향의 좌표를 모르면 다음 위치 중 하나에서 찾을 수 있을 겁니다:
<ul>
<li><a href="https://osm.org">Openstreetmap</a>
You can search for your city in the search bar.
Select the direction arrows next to the search bar. Then, drag the
green marker to the OSM map. The coords will appear in the 'from' box.
</ul>

<p>좌표 형식은 다음 중 하나입니다:
<dl>
<dt>Decimal Degrees
<dd>The format is +-DDD.DDDDDDDDDDDDDDD.  This  is  the
    format programs like xearth use and the format that
    many positioning web sites use.  However  typically
    the precision is limited to 4 or 5 decimals.
<dt>Degrees Minutes (DGM)
<dd>The  format  is +-DDDMM.MMMMMMMMMMMMM. It is not an
    arithmetic type, but a packed representation of two
    separate units, degrees and minutes. This output is
    common from some types of hand held GPS  units  and
    from NMEA format GPS messages.
<dt>Degrees Minutes Seconds (DGMS)
<dd>The  format  is +-DDDMMSS.SSSSSSSSSSS. Like DGM, it
    is not an arithmetic type but a packed  representation
    of  three separate units: degrees, minutes and
    seconds. This output is typically derived from  web
    sites  that  give 3  values for each position. For
    instance 34:50:12.24523 North might be the position
    given, in DGMS it would be +0345012.24523.
</dl>

<p>
위도 +는 북쪽, 경도 +는 동쪽입니다. 
위치가 0점에서 2도 미만이라면 사용 중인 형식을 모호하지 않게 선행 0을 충분히 지정하는 것이 중요합니다.
