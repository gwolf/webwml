#use wml::debian::template title="데비안을 선택하는 까닭" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7141cf5f25b03b1aa3c02c1afe2a5ca8cab2f672" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.

<p>여러 이유로 사용자는 데비안을 그들의 운영체제로 선택합니다.</p>

<h1>주요 이유</h1>

# try to list reasons for end users first

<dl>
  <dt><strong>데비안은 자유 소프트웨어 입니다.</strong></dt>
  <dd>
    Debian is made of free and open source software and will always be 100%
    <a href="free">free</a>. Free for anyone to use, modify, and
    distribute. This is our main promise to <a href="../users">our users</a>. It's also free of cost.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 안정적이고 안전한 리눅스 기반 운영체제입니다.</strong></dt>
  <dd>
  데비안은 랩톱, 데스크톱 및 서버를 비롯한 광범위한 장치를 위한 운영 체제입니다. 
  사용자들은 1993년 이후로 안정성과 신뢰성을 좋아했습니다. 모든 패키지에 대해 적절한 기본 구성을 제공합니다. 
  데비안 개발자는 가능한 한 평생 동안 모든 패키지에 대한 보안 업데이트를 제공합니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 넓은 하드웨어 지원을 합니다.</strong></dt>
  <dd>
    Most hardware is already supported by the Linux kernel. Proprietary drivers
    for hardware are available when the free software is not sufficient.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 부드러운 업그레이드를 지원합니다.</strong></dt>
  <dd>
    Debian is well known for its easy and smooth upgrades within a release cycle
    but also to the next major release.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 다른 많은 배포판의 씨앗이며 기반입니다.</strong></dt>
  <dd>
    Many of the most popular Linux distributions, like Ubuntu, Knoppix, PureOS,
    SteamOS or Tails, choose Debian as a base for their software.
    Debian is providing all the tools so everyone can extend the software
    packages from the Debian archive with their own packages for their needs.
  </dd>
</dl>

<dl>
  <dt><strong>데비안 프로젝트는 커뮤니티입니다.</strong></dt>
  <dd>
    Debian is not only a Linux operating system. The software is co-produced by
    hundreds of volunteers from all over the world. You can be part of the
    Debian community even if you are not a programmer or sysadmin. Debian is
    community and consensus driven and has a
    <a href="../devel/constitution">democratic governance structure</a>.
    Since all Debian developers have equal rights it cannot be controlled by
    a single company. We have developers in more than 60 countries and support
    translations for our Debian Installer in more than 80 languages.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 여러 설치 옵션이 있습니다.</strong></dt>
  <dd>
    End users will use our
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live CD</a>
    including the easy-to-use Calamares installer needing very little input or
    prior knowledge. More experienced users can use our unique full featured
    installer while experts can fine tune the installation or even use an
    automated network installation tool.
  </dd>
</dl>

<br>

<h1>기업환경</h1>

<p>
  If you need Debian in a professional environment, you may enjoy these
  additional benefits:
</p>

<dl>
  <dt><strong>데비안은 믿을 수 있습니다.</strong></dt>
  <dd>
    Debian proves its reliability every day in thousands of real world
    scenarios ranging from a single user laptop up to super colliders, stock
    exchanges and the automotive industry. It's also popular in the academic
    world, in science and the public sector.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 전문가가 많습니다.</strong></dt>
  <dd>
    Our package maintainers do not only take care of the Debian packaging and
    incorporating new upstream versions. Often they're experts in the upstream
    software and contribute to upstream development directly. Sometimes they
    are also part of upstream.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 안전합니다.</strong></dt>
  <dd>
    Debian has security support for its stable releases. Many other
    distributions and security researchers rely on Debian's security tracker.
  </dd>
</dl>

<dl>
  <dt><strong>장기 지원.</strong></dt>
  <dd>
    There's <a href="https://wiki.debian.org/LTS">Long Term Support</a>
    (LTS) at no charge. This brings you extended support for the stable
    release for 5 years and more. Besides there's also the
    <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a> initiative
    that extends supports for a limited set of packages for more than 5 years.
  </dd>
</dl>

<dl>
  <dt><strong>클라우드 이미지.</strong></dt>
  <dd>
    Official cloud images are available for all major cloud platforms. We also
    provide the tools and configuration so you can build your own customized
    cloud image. You can also use Debian in virtual machines on the desktop or
    in a container.
  </dd>
</dl>

<br>

<h1>개발자</h1>
<p>Debian is widely used by every kind of software and hardware developers.</p>

<dl>
  <dt><strong>Public available bug tracking system.</strong></dt>
  <dd>
    Our Debian <a href="../Bugs">Bug tracking system</a> (BTS) is publicly
    available for everybody via a web browser. We do not hide our software
    bugs and you can easily submit new bug reports.
  </dd>
</dl>

<dl>
  <dt><strong>IoT 및 임베디드 장비.</strong></dt>
  <dd>
    We support a wide range of devices like the Raspberry Pi, variants of QNAP,
    mobile devices, home routers and a lot of Single Board Computers (SBC).
  </dd>
</dl>

<dl>
  <dt><strong>다중 하드웨어 아키텍처.</strong></dt>
  <dd>
    Support for a <a href="../ports">long list</a> of CPU architectures
    including amd64, i386, multiple versions of ARM and MIPS, POWER7, POWER8,
    IBM System z, RISC-V. Debian is also available for older and specific
    niche architectures.
  </dd>
</dl>

<dl>
  <dt><strong>수많은 소프트웨어 사용 가능.</strong></dt>
  <dd>
    Debian has the largest number of installed packages available
    (currently <packages_in_stable>). Our packages use the deb format which is well
    known for its high quality.
  </dd>
</dl>

<dl>
  <dt><strong>다른 릴리스 선택.</strong></dt>
  <dd>
    Besides our stable release, you get the newest versions of software
    by using the testing or unstable releases.
  </dd>
</dl>

<dl>
  <dt><strong>개발자 도구 및 정책 도움으로 고품질.</strong></dt>
  <dd>
    Several developer tools help to keep the quality to a high standard and
    our <a href="../doc/debian-policy/">policy</a> defines the technical
    requirements that each package must satisfy to be included in the
    distribution. Our continuous integration is running the autopkgtest
    software, piuparts is our installation, upgrading, and removal testing
    tool and lintian is a comprehensive package checker for Debian packages.
  </dd>
</dl>

<br>

<h1>사용자들이 말하는 것</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
      For me it's the perfect level of ease of use and stability. I've used
      various different distributions over the years but Debian is the only one
      that just works.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Rock solid. Loads of packages. Excellent community.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Debian for me is symbol of stability and ease to use.
    </strong></q>
  </li>
</ul>

