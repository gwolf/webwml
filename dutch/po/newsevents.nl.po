# translation of newsevents.po to Dutch
# Templates files for webwml modules
# Copyright (C) 2003,2004 Software in the Public Interest, Inc.
#
# Frans Pop <elendil@planet.nl>, 2007, 2008.
# Jeroen Schot <schot@a-eskwadraat.nl>, 2011, 2012.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2019-2020.
#
msgid ""
msgstr ""
"Project-Id-Version: newsevents.nl.po\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-12-05 21:57+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../english/News/news.rdf.in:16
msgid "Debian News"
msgstr "Debian Nieuws"

#: ../../english/News/news.rdf.in:19
msgid "Debian Latest News"
msgstr "Laatste nieuws van Debian"

#: ../../english/News/press/press.tags:11
msgid "p<get-var page />"
msgstr "p<get-var page />"

#: ../../english/News/weekly/dwn-to-rdf.pl:143
msgid "The newsletter for the Debian community"
msgstr "De nieuwsbrief voor de Debian-gemeenschap"

#: ../../english/events/talks.defs:9
msgid "Title:"
msgstr "Titel:"

#: ../../english/events/talks.defs:12
msgid "Author:"
msgstr "Auteur:"

#: ../../english/events/talks.defs:15
msgid "Language:"
msgstr "Taal:"

#: ../../english/events/talks.defs:19
msgid "Date:"
msgstr "Datum:"

#: ../../english/events/talks.defs:23
msgid "Event:"
msgstr "Evenement:"

#: ../../english/events/talks.defs:26
msgid "Slides:"
msgstr "Dia’s:"

#: ../../english/events/talks.defs:29
msgid "source"
msgstr "broncode"

#: ../../english/events/talks.defs:32
msgid "PDF"
msgstr "PDF"

#: ../../english/events/talks.defs:35
msgid "HTML"
msgstr "HTML"

#: ../../english/events/talks.defs:38
msgid "MagicPoint"
msgstr "MagicPoint"

#: ../../english/events/talks.defs:41
msgid "Abstract"
msgstr "Samenvatting"

#: ../../english/template/debian/events_common.wml:8
msgid "Upcoming Attractions"
msgstr "Komende evenementen"

#: ../../english/template/debian/events_common.wml:11
msgid "link may no longer be valid"
msgstr "de link is misschien niet meer geldig"

#: ../../english/template/debian/events_common.wml:14
msgid "When"
msgstr "Wanneer"

#: ../../english/template/debian/events_common.wml:17
msgid "Where"
msgstr "Waar"

#: ../../english/template/debian/events_common.wml:20
msgid "More Info"
msgstr "Voor meer info"

#: ../../english/template/debian/events_common.wml:23
msgid "Debian Involvement"
msgstr "Betrokkenheid van Debian"

#: ../../english/template/debian/events_common.wml:26
msgid "Main Coordinator"
msgstr "Hoofdcoördinator"

#: ../../english/template/debian/events_common.wml:29
msgid "<th>Project</th><th>Coordinator</th>"
msgstr "<th>Project</th><th>Coördinator</th>"

#: ../../english/template/debian/events_common.wml:32
msgid "Related Links"
msgstr "Verwante sites"

#: ../../english/template/debian/events_common.wml:35
msgid "Latest News"
msgstr "Laatste nieuws"

#: ../../english/template/debian/events_common.wml:38
msgid "Download calendar entry"
msgstr "Download kalender-item"

#: ../../english/template/debian/news.wml:9
msgid ""
"Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/\">Debian "
"Project homepage</a>."
msgstr ""
"Terug naar: ander <a href=\"./\">Debian-nieuws</a> || <a href=\"m4_HOME/"
"\">startpagina van het Debian Project</a>."

#. '<get-var url />' is replaced by the URL and must not be translated.
#. In English the final line would look like "<http://broken.com (dead.link)>"
#: ../../english/template/debian/news.wml:17
msgid "<get-var url /> (dead link)"
msgstr "<get-var url /> (dode link)"

#: ../../english/template/debian/projectnews/boilerplates.wml:35
msgid ""
"Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
"the Debian community. Topics covered in this issue include:"
msgstr ""
"Welkom bij uitgave <get-var issue /> van dit jaar van DPN, de nieuwsbrief "
"voor de Debian-gemeenschap. Deze uitgave bevat onder andere de volgende "
"onderwerpen:"

#: ../../english/template/debian/projectnews/boilerplates.wml:43
msgid ""
"Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
"the Debian community."
msgstr ""
"Welkom bij uitgave <get-var issue /> van dit jaar van DPN, de nieuwsbrief "
"voor de Debian-gemeenschap."

#: ../../english/template/debian/projectnews/boilerplates.wml:49
msgid "Other topics covered in this issue include:"
msgstr "Andere onderwerpen die in dit nummer aan bod komen, zijn onder meer:"

#: ../../english/template/debian/projectnews/boilerplates.wml:69
#: ../../english/template/debian/projectnews/boilerplates.wml:90
msgid ""
"According to the <a href=\"https://udd.debian.org/bugs.cgi\">Bugs Search "
"interface of the Ultimate Debian Database</a>, the upcoming release, Debian  "
"<q><get-var release /></q>, is currently affected by <get-var testing /> "
"Release-Critical bugs. Ignoring bugs which are easily solved or on the way "
"to being solved, roughly speaking, about <get-var tobefixed /> Release-"
"Critical bugs remain to be solved for the release to happen."
msgstr ""
"Volgens de <a href=\"https://udd.debian.org/bugs.cgi\">bugzoekinterface van "
"de Ultimate Debian Database</a> is de komende release, Debian <q><get-var "
"release /></q> beinvloed door <get-var testing /> release-kritieke bugs. Als "
"we de bugs negeren die makkelijk op te lossen zijn of al bijna zijn opgelost "
"blijven er ruwweg <get-var tobefixed /> release-kritieke bugs over die "
"moeten worden opgelost om een release mogelijk te maken."

#: ../../english/template/debian/projectnews/boilerplates.wml:70
msgid ""
"There are also some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">hints on how to interpret</a> these numbers."
msgstr ""
"Er zijn ook enkele < <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">hints over het interpreteren</a> van deze getallen."

#: ../../english/template/debian/projectnews/boilerplates.wml:91
msgid ""
"There are also <a href=\"<get-var url />\">more detailed statistics</a> as "
"well as some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats\">hints "
"on how to interpret</a> these numbers."
msgstr ""
"Er zijn ook <a href=\"<get-var url />\">gedetaileerdere statistieken</a> "
"alsook enkele <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats\">hints "
"over het interpreteren</a> van deze getallen."

#: ../../english/template/debian/projectnews/boilerplates.wml:115
msgid ""
"<a href=\"<get-var link />\">Currently</a> <a href=\"m4_DEVEL/wnpp/orphaned"
"\"><get-var orphaned /> packages are orphaned</a> and <a href=\"m4_DEVEL/"
"wnpp/rfa\"><get-var rfa /> packages are up for adoption</a>: please visit "
"the complete list of <a href=\"m4_DEVEL/wnpp/help_requested\">packages which "
"need your help</a>."
msgstr ""
"Er zijn <a href=\"<get-var link />\">op dit moment</a> <a href=\"m4_DEVEL/"
"wnpp/orphaned\"><get-var orphaned /> verweesde pakketten</a> en <a href="
"\"m4_DEVEL/wnpp/rfa\"><get-var rfa /> pakketten aangeboden voor adoptie</a>: "
"bezoek de volledige lijst van  <a href=\"m4_DEVEL/wnpp/help_requested"
"\">pakketten die uw hulp nodig hebben</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:127
msgid ""
"Please help us create this newsletter. We still need more volunteer writers "
"to watch the Debian community and report about what is going on. Please see "
"the <a href=\"https://wiki.debian.org/ProjectNews/HowToContribute"
"\">contributing page</a> to find out how to help. We're looking forward to "
"receiving your mail at <a href=\"mailto:debian-publicity@lists.debian.org"
"\">debian-publicity@lists.debian.org</a>."
msgstr ""
"Help ons bij het maken van deze nieuwsbrief. We hebben nog steeds "
"vrijwilligers nodig die schrijven over wat er speelt in de Debian-"
"gemeenschap. Bekijk de <a href=\"https://wiki.debian.org/ProjectNews/"
"HowToContribute\">bijdragen-pagina</a> om te lezen hoe u kunt helpen. We "
"verheugen ons op uw e-mail op <a href=\"mailto:debian-publicity@lists.debian."
"org\">debian-publicity@lists.debian.org</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:188
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a>) for "
"announcements."
msgstr ""
"Deze lijst is slechts een selectie van de belangrijkere "
"beveiligingsaanbevelingen van de afgelopen week. Als u op de hoogte gehouden "
"moet worden over beveiligingsaanbevelingen die uitgebracht zijn door het "
"Debian beveiligingsteam, abonneert u zich dan op de <a href=\"<get-var url-"
"dsa />\">beveiligings e-maillijst</a> (en de aparte <a href=\"<get-var url-"
"bpo />\">backports lijst</a> en <a href=\"<get-var url-stable-announce />"
"\">stabiele updates lijst</a>) voor aankondigingen."

#: ../../english/template/debian/projectnews/boilerplates.wml:189
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a> or <a href="
"\"<get-var url-volatile-announce />\">volatile list</a>, for <q><get-var old-"
"stable /></q>, the oldstable distribution) for announcements."
msgstr ""
"Deze lijst is slechts een selectie van de belangrijkere "
"beveiligingsaanbevelingen van de afgelopen week. Als u op de hoogte gehouden "
"moet worden over beveiligingsaanbevelingen die uitgebracht zijn door het "
"Debian beveiligingsteam, abonneert u zich dan op de <a href=\"<get-var url-"
"dsa />\">beveiligings e-maillijst</a> (en de aparte <a href=\"<get-var url-"
"bpo />\">backports lijst</a> en <a href=\"<get-var url-stable-announce />"
"\">stabiele updates lijst</a> of <a href=\"<get-var url-volatile-announce />"
"\">volatile lijst</a> voor <q><get-var old-stable /></q>, de oldstable "
"distributie) voor aankondigingen."

#: ../../english/template/debian/projectnews/boilerplates.wml:198
msgid ""
"Debian's Stable Release Team released an update announcement for the "
"package: "
msgstr ""
"Debians Stable Release Team heeft een update aankondiging gedaan voor het "
"pakket: "

#: ../../english/template/debian/projectnews/boilerplates.wml:200
#: ../../english/template/debian/projectnews/boilerplates.wml:213
#: ../../english/template/debian/projectnews/boilerplates.wml:226
#: ../../english/template/debian/projectnews/boilerplates.wml:357
#: ../../english/template/debian/projectnews/boilerplates.wml:371
msgid ", "
msgstr ","

#: ../../english/template/debian/projectnews/boilerplates.wml:201
#: ../../english/template/debian/projectnews/boilerplates.wml:214
#: ../../english/template/debian/projectnews/boilerplates.wml:227
#: ../../english/template/debian/projectnews/boilerplates.wml:358
#: ../../english/template/debian/projectnews/boilerplates.wml:372
msgid " and "
msgstr "en"

#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
#: ../../english/template/debian/projectnews/boilerplates.wml:228
msgid ". "
msgstr "."

#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
#: ../../english/template/debian/projectnews/boilerplates.wml:228
msgid "Please read them carefully and take the proper measures."
msgstr "Leest u ze a.u.b zorgvuldig en neem de juiste maatregelen."

#: ../../english/template/debian/projectnews/boilerplates.wml:211
msgid "Debian's Backports Team released advisories for these packages: "
msgstr ""
"Debians Backports Team heeft adviezen uitgebracht voor deze pakketten: "

#: ../../english/template/debian/projectnews/boilerplates.wml:224
msgid ""
"Debian's Security Team recently released advisories for these packages "
"(among others): "
msgstr ""
"Debians Beveiligings Team heeft recentelijk adviezen uitgebracht voor (onder "
"andere) deze pakketten: "

#: ../../english/template/debian/projectnews/boilerplates.wml:253
msgid ""
"<get-var num-newpkg /> packages were added to the unstable Debian archive "
"recently."
msgstr ""
"<get-var num-newpkg /> pakketten zijn recentelijk toegevoegd aan het "
"unstable Debian archief."

#: ../../english/template/debian/projectnews/boilerplates.wml:255
msgid " <a href=\"<get-var url-newpkg />\">Among many others</a> are:"
msgstr " <a href=\"<get-var url-newpkg />\">Naast vele andere</a> zijn:"

#: ../../english/template/debian/projectnews/boilerplates.wml:282
msgid "There are several upcoming Debian-related events:"
msgstr "Er komen verschillende Debian gerelateerde evenementen aan:"

#: ../../english/template/debian/projectnews/boilerplates.wml:288
msgid ""
"You can find more information about Debian-related events and talks on the "
"<a href=\"<get-var events-section />\">events section</a> of the Debian web "
"site, or subscribe to one of our events mailing lists for different regions: "
"<a href=\"<get-var events-ml-eu />\">Europe</a>, <a href=\"<get-var events-"
"ml-nl />\">Netherlands</a>, <a href=\"<get-var events-ml-ha />\">Hispanic "
"America</a>, <a href=\"<get-var events-ml-na />\">North America</a>."
msgstr ""
"U kunt meer informatie over Debian-gerelateerde evenementen en praatjes "
"vinden in de <a href=\"<get-var events-section />\">evenementen sektie</a> "
"van de Debian web-site, of abonneer u op een van onze evenementen e-"
"maillijsten voor verschillende regios: <a href=\"<get-var events-ml-eu />"
"\">Europa</a>, <a href=\"<get-var events-ml-nl />\">Nederland</a>, <a href="
"\"<get-var events-ml-ha />\">Spaans Amerika</a>, <a href=\"<get-var events-"
"ml-na />\">Noord Amerika</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:313
msgid ""
"Do you want to organise a Debian booth or a Debian install party? Are you "
"aware of other upcoming Debian-related events? Have you delivered a Debian "
"talk that you want to link on our <a href=\"<get-var events-talks />\">talks "
"page</a>? Send an email to the <a href=\"<get-var events-team />\">Debian "
"Events Team</a>."
msgstr ""
"Wilt u een Debian stand of een Debian installatiefeest organiseren? Bent u "
"bekend met een aankomend Debian-gerelateerd evenement? Heeft u een verhaal "
"gehouden over Debian dat u wilt linken op onze <a href=\"<get-var events-"
"talks />\">verhalen pagina</a>? Stuurt u dan een e-mail aan het <a href="
"\"<get-var events-team />\">Debian evenementen team</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:335
msgid ""
"<get-var dd-num /> applicants have been <a href=\"<get-var dd-url />"
"\">accepted</a> as Debian Developers"
msgstr ""
"<get-var dd-num /> sollicitanten zijn <a href=\"<get-var dd-url />"
"\">geaccepteerd</a> als Debian Developers"

#: ../../english/template/debian/projectnews/boilerplates.wml:342
msgid ""
"<get-var dm-num /> applicants have been <a href=\"<get-var dm-url />"
"\">accepted</a> as Debian Maintainers"
msgstr ""
"<get-var dm-num /> sollicitanten zijn <a href=\"<get-var dm-url />"
"\">geaccepteerd</a> als Debian Maintainers"

#: ../../english/template/debian/projectnews/boilerplates.wml:349
msgid ""
"<get-var uploader-num /> people have <a href=\"<get-var uploader-url />"
"\">started to maintain packages</a>"
msgstr ""
"<get-var uploader-num /> mensen zijn <a href=\"<get-var uploader-url />"
"\">begonnen met het beheren van pakketten</a>"

#: ../../english/template/debian/projectnews/boilerplates.wml:394
msgid ""
"<get-var eval-newcontributors-text-list /> since the previous issue of the "
"Debian Project News. Please welcome <get-var eval-newcontributors-name-list /"
"> into our project!"
msgstr ""
"<get-var eval-newcontributors-text-list /> sinds de vorige editie van het "
"Debian project nieuws. Laten we <get-var eval-newcontributors-name-list /> "
"verwelkomen in ons project!"

#: ../../english/template/debian/projectnews/boilerplates.wml:407
msgid ""
"The <get-var issue-devel-news /> issue of the <a href=\"<get-var url-devel-"
"news />\">miscellaneous news for developers</a> has been released and covers "
"the following topics:"
msgstr ""
"De <get-var issue-devel-news /> editie van het <a href=\"<get-var url-devel-"
"news />\">diverse nieuws voor ontwikkelaars</a> is uitgegeven en behandelt "
"de volgende onderwerpen:"

#: ../../english/template/debian/projectnews/footer.wml:6
msgid ""
"To receive this newsletter in your mailbox, <a href=\"https://lists.debian."
"org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"Om deze nieuwsbrief in uw mailbox te ontvangen, kunt u zich <a href="
"\"https://lists.debian.org/debian-news/\">aanmelden voor de debian-news "
"mailinglijst</a>."

#: ../../english/template/debian/projectnews/footer.wml:10
#: ../../english/template/debian/weeklynews/footer.wml:10
msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
msgstr ""
"<a href=\"../../\">Eerdere uitgaven</a> van deze nieuwsbrief zijn "
"beschikbaar."

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:15
msgid ""
"<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Debian Projectnieuws wordt geredigeerd door <a href="
"\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:20
msgid ""
"<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Debian Projectnieuws wordt geredigeerd door <a href="
"\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:25
msgid ""
"<void id=\"singular\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Deze uitgave van Debian Projectnieuws is geredigeerd "
"door <a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:30
msgid ""
"<void id=\"plural\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Deze uitgave van Debian Projectnieuws is geredigeerd "
"door <a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. One translator only
#. One translator only
#: ../../english/template/debian/projectnews/footer.wml:35
#: ../../english/template/debian/weeklynews/footer.wml:35
msgid "<void id=\"singular\" />It was translated by %s."
msgstr ""
"<void id=\"singular\" />Deze editie van Debian Weekly News is vertaald door "
"%s."

#. Two ore more translators
#. Two ore more translators
#: ../../english/template/debian/projectnews/footer.wml:40
#: ../../english/template/debian/weeklynews/footer.wml:40
msgid "<void id=\"plural\" />It was translated by %s."
msgstr ""
"<void id=\"plural\" />Deze editie van Debian Weekly News is vertaald door %s."

#. One female translator only
#. One female translator only
#: ../../english/template/debian/projectnews/footer.wml:45
#: ../../english/template/debian/weeklynews/footer.wml:45
msgid "<void id=\"singularfemale\" />It was translated by %s."
msgstr ""
"<void id=\"singularfemale\" />Deze editie van Debian Weekly News is vertaald "
"door %s."

#. Two ore more female translators
#. Two ore more female translators
#: ../../english/template/debian/projectnews/footer.wml:50
#: ../../english/template/debian/weeklynews/footer.wml:50
msgid "<void id=\"pluralfemale\" />It was translated by %s."
msgstr ""
"<void id=\"pluralfemale\" />Deze editie van Debian Weekly News is vertaald "
"door %s."

#: ../../english/template/debian/weeklynews/footer.wml:6
msgid ""
"To receive this newsletter weekly in your mailbox, <a href=\"https://lists."
"debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"Om deze nieuwsbrief wekelijks in uw mailbox te ontvangen, kunt u zich <a "
"href=\"https://lists.debian.org/debian-news/\">aanmelden voor de debian-news "
"mailinglijst</a>."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:15
msgid ""
"<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Debian Weekly News wordt geredigeerd door <a href="
"\"mailto:dwn@debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:20
msgid ""
"<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Debian Weekly News wordt geredigeerd door <a href="
"\"mailto:dwn@debian.org\">%s</a>."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:25
msgid ""
"<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
"href=\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Deze uitgave Debian Weekly News werd geredigeerd "
"door <a href=\"mailto:dwn@debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:30
msgid ""
"<void id=\"plural\" />This issue of Debian Weekly News was edited by <a href="
"\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Deze uitgave van Debian Weekly News werd geredigeerd "
"door <a href=\"mailto:dwn@debian.org\">%s</a>."
