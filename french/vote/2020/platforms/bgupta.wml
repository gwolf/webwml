#use wml::debian::template title="Programme de Brian Gupta" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::translation-check translation="f7b3df3fb50baf4ac074c1bec9bc2b7908799a9f" maintainer="Jean-Pierre Giraud"

<h2>Introduction</h2>
<p>
Je candidate au poste de chef du projet Debian (DPL) avec un but unique, la
création des fondations Debian US et Debian Europe. Je vois essentiellement ma
candidature comme un référendum sur cet objectif et ses détails.
</p>
<p>
De façon opérationnelle, je reconstituerai l'équipe d'assistance du DPL pour
assister le responsable du projet.
</p>
<h2>Justification</h2>
<p>
La justification de ces modifications est de permettre à Debian d'avancer plus
vite sur ses problèmes qu'ils soient financiers, légaux ou liés aux marques
déposées. Le DPL devrait avoir le choix de déléguer certaines de ses fonctions
administratives afin de pouvoir se concentrer sur la fixation de
l'orientation du projet, sa représentation et obtenir l'adhésion des membres
pour faire avancer les questions importantes. Par exemple : les chefs du projet
Debian devraient avoir plus de temps pour piloter plutôt que de s'enterrer dans
l'ensemble des tâches administratives auxquels ils font face actuellement et
l'histoire a montré que des volontaires seuls ne suffisent pas.
</p>
<p>
Pourquoi ne pouvons-nous pas faire tout cela avec des volontaires, comme nous
faisons tout le reste ? Même si pour beaucoup de tâches techniques, il est
plutôt facile de faire le travail dans le cadre de notre travail salarié
quotidien ou de celui d'autres rôles techniques que nous pouvons avoir, parce
que les compétences nécessaires sont souvent naturellement adaptées aux
techniciens, pour les autres tâches non techniques, comme la comptabilité, les
questions juridiques ou autres, il est très difficile de trouver assez de
personnes volontaires pour mener ces tâches et prêtes à en faire une priorité.
À tout le moins, le vivier de volontaires qualifiés intéressés à ces problèmes,
mais qui sont aussi des techniciens intéressés à Debian, est beaucoup plus
petit que celui des volontaires à des tâches techniques ou plus créatives.
</p>
<p>
Pourquoi créer nos propres organismes alors que nous en avons déjà ? Nous avons
des résultats mitigés sur les années de travail avec des organismes habilités
(« Trusted Organizations ») qui ne sont pas concentrés sur Debian. Ce n'est
pas un secret que SPI est le principal organisme habilité. Il est aussi
relativement bien connu que la qualité du service rendu à Debian par SPI a
beaucoup varié au fil des ans.
</p>
<p>
Actuellement, le niveau de service de SPI s'est largement amélioré, grâce à
leur utilisation d'une assistance payée, mais il fut un temps où le service
fourni était si mauvais qu'il était difficile de justifier de maintenir cette
relation privilégiée. Notez que cela était en grande partie dû à des cycles de
volontaires limités, allié à un mandat très large, pour prendre en charge de
nombreux projets.
</p>
<p>
Le plus important défi récent est que SPI a connu un changement de
philosophie. SPI a indiqué qu'il ne pense plus que Debian, qui est le projet
membre fondateur de SPI, est leur projet le plus important et que tous les
projets doivent être traités de la même manière.
</p>
<p>
Cela se manifeste de différentes manières : 1) le DPL n'est plus un membre
spécial de SPI, invité à toutes les réunions 2) sans nous informer, après dix
ans de pratique de fait, SPI a cessé d'exonérer les versements des parrainages
des DebConf des 5 % de frais standard, ce qui a coûté à Debian environ 16 000 $
depuis que cette modification, non annoncée, est devenue effective, 3) SPI
aimerait fermer le compte PayPal de Debian, pour la simple raison qu'il ne peut
pas offrir à tous les projets membres leurs propres comptes PayPal. (Un autre
grand projet membre de SPI, PostgreSQL, a ses propres fondations pour résoudre
ces problèmes.)
</p>
<p>
En complément, notre organisme habilité européen préféré de façon historique,
le FFIS, est devenu non fonctionnel à toutes fins pratiques, au point que,
désormais, nous demandons aux gens de ne plus faire de donation à travers le
FFIS, et nous l'avons retiré de la liste des organismes habilités actuels.
</p>
<h2>Vue d'ensemble</h2>
<p>
Lors de la création de ces fondations, je commencerai par Debian US, parce que
je suis basé aux États-Unis (New York). Le bureau de la fondation sera élu par
les membres du projet Debian, et le DPL sera automatiquement un membre à part
entière du bureau de la fondation. (Dans un premier temps, un bureau provisoire
pourra être rassemblé par le DPL pour démarrer.) Les membres du bureau devront
être des membres du projet Debian mais sans aucune restriction d'ordre
géographique.
</p>
<p>
Dès que la fondation sera opérationnelle, un objectif initial de la fondation
sera d'embaucher un administrateur à temps partiel (20 heures par semaine) qui
suivra les instructions du responsable du projet Debian et du bureau, et qui
pourra assister le responsable du projet et les différentes équipes
administratives y compris, mais pas seulement, l'équipe d'assistance du
responsable du projet, celle de trésorerie et celle en charge des marques
déposées.
</p>
<p>
Un autre objectif sera de constituer Debian Europe. La France et la Suisse
seront au premier rang des territoires sur lesquels établir une fondation
dans la mesure où nous y avons des organismes agréés solides. Je me
coordonnerai avec eux et verrai s'ils envisagent de faire évoluer leurs
organismes en une entité à l'échelle de l'Europe ou aider à la fondation
d'une nouvelle entité.
</p>
<p>
Avec le temps, je m'attendrai à ce que ces deux entités deviennent nos
organismes habilités principaux aux USA et en Europe sans pour autant mettre
fin aux relations avec nos organismes habilités actuels, SPI, Debian France et
Debian.ch.
</p>
<p>
Il est possible de trouver des entités similaires en se tournant vers la
fondation FreeBSD, la « United States PostgreSQL Association » et « PostgreSQL
Europe ».
</p>
<p>
Si vous partagez mes objectifs pour fonder ces entités, vous êtes encouragés à
me joindre et me le faire savoir en indiquant si vous pouvez aider.
</p>
<h2>Engagements pris pendant la campagne</h2>
<p>
Je remplirai les obligations attendues de tous les DPL :
</p>
<ul>
  <li>Rédaction et publication de rapports mensuels. Les Brèves du chef de
projet Debian sont devenus une source essentielle de nouvelles pour les membres
du projet et je suis enthousiaste à l'idée de poursuivre cette tradition.</li>
  <li>Révision et autorisation des délégations.
  <li>J'ai l'intention de joindre immédiatement toutes les équipes déléguées
pour programmer leur disponibilité durant l'année à venir. Ainsi, si des
équipes ont besoin d'une attention particulière du DPL à certains moments, je
la réserverai à ces équipes.</li>
  <li>Prendre des décisions sur les propriétés détenues en fiducie pour Debian
(essentiellement en examinant et approuvant les dépenses et les budgets dans
les délais opportuns).</li>
  <li>Apporter mon soutien au comité technique en nommant de nouveaux membres.
  <li>Ajouter et retirer des organismes de la liste des organismes habilités.
Actuellement, je n'ai pas d'autres mesures urgentes à prendre sur ce front que
d’œuvrer à la création des nouvelles Fondations.</li>
  <li>Solliciter des avis à la fois en privé et de façon publique.</li>
  <li>Être disponible pour tous les développeurs qui ont besoin d'aide.</li>
</ul>
<p>
En complément, je m'engage à suivre cette ligne de conduite au sujet des
fondations :
</p>
<ul>
  <li>Recruter une équipe d'au moins trois développeurs Debian supplémentaires
pour travailler avec moi sur le projet de fondation(s) (offre ouverte à tous)</li>
  <li>Travailler avec l'équipe pour faire le gros du travail et des recherches
nécessaires pour avoir une discussion approfondie avec le projet sur les étapes
suivantes.</li>
  <li>Je rechercherai le consensus sur « debian-project » au sujet des détails
sur la ou les fondations, avec l'objectif de construire un consensus sur la
création des fondations aux États-Unis et en Europe, qui nous permettent
d'embaucher directement des services financiers, juridiques et administratifs
salariés et des employés.</li>
  <li>Je viserai une résolution générale et à bâtir un consensus pour toutes
les modifications de notre constitution rendues légalement nécessaires pour
mettre en œuvre les fondations.</li>
  <li>Je consulterai les développeurs Debian sur l'embauche de personnels (et
sur les autres dépenses importantes).</li>
</ul>
<p>
En complément, j'ai les objectifs et plans suivants qui ne sont pas liés aux
à la fondation :
</p>
<ul>
  <li>Je vais recréer les assistants DPL et inviter les membres du projet et
les autres candidats au poste de DPL (passés et présents) à me rejoindre. Je
prévois de rétablir les réunions régulières de l'équipe du DPL toutes les deux
semaines. Les réunions se tiendront sur IRC et seront ouvertes à tous.</li>
  <li>Je travaillerai avec les responsables des comptes de Debian (DAM) et les
responsables du site web pour séparer les droits de téléversement du parcours
pour devenir développeur Debian. Devenir développeur Debian empaqueteur est
tenu comme le parcours par défaut, ce qui décourage de potentiels candidats de
viser au statut de développeur Debian avant d'être prêts à subir les
vérifications de tâches et de compétences.</li>
  <li>Je m'assurerai que toutes les délégations sont conformes à la
constitution (nous en avons au moins deux qui ne sont pas conformes).</li>
  <li>Tout en reconnaissant que beaucoup de gens ne sont pas prêts au
changement, j'encouragerai l'expérimentation dans le domaine des technologies
de la communication, par exemple - conversation et outils de téléprésence.</li>
  <li>J'assisterai à toutes les conférences Debian durant mon mandat (sauf s'il
y a des restrictions de voyage). Les DebConf sont importantes et il est
important que le DPL y assiste.</li>
  <li>Je candidaterai à nouveau si tout se passe bien et que les gens sont
satisfaits de mon travail. Je dis cela parce que tous les anciens DPL
confirmeront que cela prend du temps d'apprendre à être DPL. Il est tout à fait
logique de permettre au projet de profiter de cette expérience à travers un
second mandat.</li>
</ul>

<h2>À propos de moi</h2>
<p>
Je suis impliqué dans la communauté du logiciel libre depuis près de quinze
ans, membre du projet Debian depuis presque sept ans et j'ai piloté le groupe
d'utilisateurs de Linux de New York (NYLUG) pendant plus de 10 ans. Mon
engagement dans Debian a débuté en 2008, en tant qu'un des premiers membres de
l'équipe de candidature à ce qui est devenu la DebConf10. J'ai également été
impliqué dans l'équipe de trésorerie, celle en charge des marques déposées
et celle des assistants DPL. J'ai collecté des fonds pour Debian et les
Debconf pendant des années et aidé Debian à obtenir un compte PayPal à travers
SPI. J'espère avoir au moins 10 à 15 heures par semaine pour travailler aux
tâches de responsable du projet Debian. J'estime qu'il faudra entre 6 et
12 mois pour créer l'entité aux USA.
</p>

<h2>Réfutations</h2>
<h3>Jonathan Carter</h3>
J'aimerais féliciter Jonathan pour avoir de nouveau candidaté au poste de DPL :
c'est moins courant que cela ne devrait l'être.
<p>
<strong>Nomenclature des membres :</strong> 
</p>
<p>
Je ne crois pas que nous devrions refuser désormais d'utiliser le terme de
développeur Debian parce que certains l'ont détourné pour se représenter
fallacieusement.
</p>
<p>
En tant que membre de la minorité des développeurs Debian qui ont rejoint le
projet pour des contributions qui ne sont pas liées à l'empaquetage, quand j'ai
rejoint le projet à l'origine, j'avais une nette préférence pour le terme de
membre du projet Debian. Néanmoins, j'en suis venu à comprendre que ma
réticence initiale à utiliser le terme de développeur Debian était une
manifestation du « syndrome de l'imposteur », et j'ai fini par utiliser ces
deux termes de façon interchangeable, selon les personnes à qui je m'adressais.
J'utilise rarement le terme de développeur Debian non empaqueteur, mais je le
fais quand cela est nécessaire pour être plus clair. Mon choix dépend
largement sur ce qui sera le mieux compris ou le plus utile selon le contexte.
</p>
<p>
Je suis un peu découragé de voir que Jonathan a choisi de retarder son adhésion
au projet pour obtenir des pleins droits de téléversement. Je n'en veux pas à
Jonathan pour cela parce que j'ai vu d'autres personnes exprimer cette
préférence auparavant, mais cela me semble être le signe d'un problème plus
important, et cela m'amène à me demander s'il n'y a pas une sorte de
stigmatisation du statut de développeur Debian non empaqueteur, au moins chez
ceux qui souhaitent adhérer au projet.
</p>
<p>
Le futur à long terme idéal pour Debian, à mon avis, devrait être celui d'un
projet où une large majorité de développeurs n'auraient de droits de
téléversement que pour les paquets dont ils sont responsables, dans la mesure
où je ne crois pas que 1000 personnes ont tous besoin de droits de
téléversement pour 60 000 paquets, et pense que ces droits devraient être très
sélectifs et qu'ils sont seulement nécessaires pour les développeurs qui ont
des responsabilités sur tout le projet, par exemple, pour les membres actifs de
l'équipe de sécurité de Debian.
</p>
<p>
Dans l'idéal, ces droits devraient s’aligner sur ceux dans Salsa. Cela n'a pas
vraiment de sens qu'il soit possible de téléverser un paquet alors qu'on ne
peut pas modifier le source et qu'il soit possible de modifier le source d'un
paquet (par exemple, le groupe Debian = collab-maint) mais pas de le
télécharger au moins vers une file d'attente.
</p>
<p>
Je comprends qu'un tel changement ne soit pas réalisable aujourd'hui et que
nous soyons très loin d'obtenir un consensus, aussi, ce n'est PAS un objectif
que je poursuis activement, mais plutôt une idée que je sème et je présume que
c'est une évolution que nous devrons mettre en place graduellement sur une
longue période.
</p>
<p>
Il est clair que nous devons y travailler en tant que projet, mais je ne crois
pas que c'est par une résolution générale que nous déciderons de nous appeler
des développeurs Debian. Je préférerais continuer à laisser ce choix à chaque
développeur et travailler avec l'équipe des responsables des comptes de Debian
et celle en charge du site web pour s'assurer que le processus et la
documentation pour la candidature au projet dissocient fortement l'adhésion et
le droit de téléversement sur l'ensemble du projet. (Actuellement, le statut de
développeur non empaqueteur est traité comme une anomalie ou une exception, ce
qui peut contribuer à cette perception.)
</p>

<h3>Sruthi Chandran</h3>
<p>
J'aimerais féliciter Sruthi pour sa candidature. Même si je n'ai pas de
problème avec les objectifs élevés qu'elle a exprimés, j'aimerais vraiment en
connaître le détail pour pouvoir pleinement les évaluer. Sruthi pilote la
préparation de la DebConf 22. Même si ce n'est que dans deux ans, il y a
beaucoup de travaux préparatoires à réaliser et beaucoup sont assortis de
délais. Aussi, on s'attend en général à ce que les équipes candidates à de
futures DebConf s'impliquent fortement dans la préparation des DebConf qui se
tiennent entre temps. J'encouragerais personnellement Sruthi à se présenter au
poste de DPL après la DebConf 22, dans la mesure où elle n'aurait plus à
arbitrer entre ces deux tâches dans son emploi du temps et où cela lui
donnerait plus d'expérience du travail dans le projet Debian.
</p>
