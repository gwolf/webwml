#use wml::debian::translation-check translation="6deb41e83064921e63d318734179cf3b4d8867e0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Un défaut a été découvert dans coturn, un serveur TURN et STUN de voix
sur IP. Par défaut coturn n'autorise pas de pairs avec les adresses de la
boucle locale (127.x.x.x et ::1). Un attaquant distant peut contourner la
protection à l'aide d'une requête contrefaite pour l'occasion utilisant une
adresse de pair de <q>0.0.0.0</q> pour forcer coturn à créer un relais vers
l'interface de la boucle locale. Si l'interface est à l'écoute sur IPv6,
elle peut aussi être atteinte en utilisant [::1] ou [::] comme adresse.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 4.5.1.1-1.1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets coturn.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de coturn, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/coturn">\
https://security-tracker.debian.org/tracker/coturn</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4829.data"
# $Id: $
