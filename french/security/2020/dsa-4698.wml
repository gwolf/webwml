#use wml::debian::translation-check translation="a871b5fd7d3849d5e29ce5a2e7e4dd60e8408d58" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation de privilèges, un déni de service ou
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-2182">CVE-2019-2182</a>

<p>Hanjun Guo et Lei Li ont signalé une situation de compétition dans le
code de gestion de la mémoire virtuel d'arm64 qui pourrait conduire à une
divulgation d'informations, un déni de service (plantage), ou
éventuellement à une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5108">CVE-2019-5108</a>

<p>Mitchell Frank de Cisco a découvert que quand la pile IEEE 802.11 (WiFi)
était utilisée en mode accès sans fil (AP) avec itinérance, elle pouvait
déclencher l'itinérance pour une nouvelle station associée avant que la
station ne soit authentifiée. Un attaquant dans la portée du point d'accès
pourrait utiliser cela pour provoquer un déni de service, soit en
remplissant une table de commutateur soit en redirigeant le trafic loin
d'autres stations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19319">CVE-2019-19319</a>

<p>Jungyeon a découvert qu'un système de fichiers contrefait peut faire que
l'implémentation de ext4 désalloue ou réalloue des blocs de journal. Un
utilisateur autorisé à monter des systèmes de fichiers pourrait utiliser
cela pour provoquer un déni de service (plantage), ou, éventuellement, pour
une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19462">CVE-2019-19462</a>

<p>L'outil syzbot a découvert une vérification d’erreur manquante dans la
bibliothèque <q>relay</q> utilisée pour implémenter divers fichiers sous
debugfs. Un utilisateur local autorisé à accéder à debugfs pourrait
utiliser cela pour provoquer un déni de service (plantage) ou
éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19768">CVE-2019-19768</a>

<p>Tristan Madani a signalé une situation de compétition dans l'utilitaire
de débogage blktrace qui pourrait avoir pour conséquence une utilisation de
mémoire après libération. Un utilisateur local capable de déclencher le
retrait de périphériques de type bloc, pourrait éventuellement utiliser
cela pour provoquer un déni de service (plantage) ou pour une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20806">CVE-2019-20806</a>

<p>Un potentiel déréférencement de pointeur NULL a été découvert dans le
pilote média tw5864. L'impact de sécurité de cela n'est pas clair.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20811">CVE-2019-20811</a>

<p>L'outil Hulk Robot a découvert un bogue de compte de références dans une
erreur de chemin dans le sous-système réseau. L'impact de sécurité de cela
n'est pas clair.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0543">CVE-2020-0543</a>

<p>Des chercheurs de l'Université libre d'Amsterdam ont découvert que, sur
certains processeurs Intel prenant en charge les instructions RDRAND et
RDSEED, une partie d'une valeur aléatoire générée par ces instructions
pouvait être utilisée dans une exécution spéculative ultérieure dans
n'importe quel cœur du même processeur physique. Selon la manière dont ces
instructions sont utilisées par les applications, un utilisateur local ou
une machine virtuelle pourrait utiliser cela pour obtenir des informations
sensibles telles que des clés de chiffrement d'autres utilisateurs ou
machines virtuelles.</p>

<p>Cette vulnérabilité peut être atténuée par une mise à jour du microcode,
soit dans le cadre des microprogrammes du système (BIOS) ou au moyen du
paquet intel-microcode de la section non-free de l'archive Debian. Cette
mise à jour du noyau ne fournit que le rapport de la vulnérabilité et
l'option de désactiver la mitigation si elle n'est pas nécessaire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-2732">CVE-2020-2732</a>

<p>Paulo Bonzini a découvert que l'implémentation de KVM pour les
processeurs Intel ne gérait pas correctement l'émulation d'instruction pour
les clients L2 quand la virtualisation imbriquée est activée. Cela
pourrait permettre à un client L2 de provoquer une élévation de privilèges,
un déni de service, ou des fuites d'informations dans le client L1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8428">CVE-2020-8428</a>

<p>Al Viro a découvert une possible utilisation de mémoire après libération
dans le cœur du système de fichiers (vfs). Un utilisateur local pourrait
exploiter cela pour provoquer un déni de service (plantage) ou
éventuellement pour obtenir des informations sensibles du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8647">CVE-2020-8647</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2020-8649">CVE-2020-8649</a>

<p>L’outil Hulk Robot a trouvé un possible accès MMIO hors limites dans le
pilote vgacon. Un utilisateur local autorisé à accéder à un terminal virtuel
(/dev/tty1, etc.) sur un sytème utilisant le pilote vgacon pourrait utiliser
cela pour provoquer un déni de service (plantage ou corruption de mémoire) ou
éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8648">CVE-2020-8648</a>

<p>L'outil syzbot a découvert une situation de compétition dans le pilote
de terminal virtuel qui pourrait avoir pour conséquence une utilisation de
mémoire après libération. Un utilisateur local autorisé à accéder à un
terminal virtuel pourrait utiliser cela pour provoquer un déni de service
(plantage ou corruption de mémoire) ou éventuellement pour une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9383">CVE-2020-9383</a>

<p>Jordy Zomer a signalé une vérification incorrecte d’intervalle dans le
pilote de disquette qui pourrait conduire à un accès statique hors limites.
Un utilisateur local autorisé à accéder à un pilote de disquette pourrait
utiliser cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou, éventuellement, pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10711">CVE-2020-10711</a>

<p>Matthew Sheets a signalé des problèmes de déréférencement de pointeur
NULL dans le sous-système SELinux lors de la réception de paquet CIPSO avec
une catégorie vide. Un attaquant distant peut tirer avantage de ce défaut
pour provoquer un déni de service (plantage). Notez que ce problème
n'affecte pas les paquets binaires distribués dans Debian dans la mesure où
CONFIG_NETLABEL n'est pas activé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10732">CVE-2020-10732</a>

<p>Une fuite d'informations de la mémoire privée du noyau vers l'espace
utilisateur a été découverte dans l'implémentation du noyau du vidage de
mémoire des processus de l'espace utilisateur .</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10751">CVE-2020-10751</a>

<p>Dmitry Vyukov a signalé que le sous-système SELinux ne gérait pas
correctement la validation de messages multiples, ce qui pourrait permettre
à un attaquant privilégié de contourner les restrictions netlink de
SELinux.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10757">CVE-2020-10757</a>

<p>Fan Yang a signalé un défaut dans la manière dont mremap gérait les très
grandes pages DAX, permettant une élévation des privilèges d'un utilisateur
local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10942">CVE-2020-10942</a>

<p>Le pilote vhost_net driver ne validait pas correctement le type de
sockets définis comme dorsaux. Un utilisateur local autorisé à accéder
à /dev/vhost-net pourrait utiliser cela pour provoquer une corruption de pile
à l’aide d’appels système contrefaits, avec pour conséquence un déni de
service (plantage) ou éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11494">CVE-2020-11494</a>

<p>Le pilote de réseau (serial line CAN) n’initialisait pas entièrement les
en-têtes CAN pour les paquets reçus, avec pour conséquence une fuite
d'informations du noyau vers l'espace utilisateur ou sur le réseau CAN.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11565">CVE-2020-11565</a>

<p>Entropy Moe a signalé que le système de fichiers de la mémoire partagée
(tmpfs) ne gérait pas correctement une option de montage <q>mpol</q>
indiquant une liste de nœuds vide, conduisant à une écriture  hors limites
basée sur la pile. Si les espaces de noms utilisateur sont activés, un
utilisateur local pourrait utiliser cela pour provoquer un déni de service
(plantage) ou éventuellement pour élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11608">CVE-2020-11608</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2020-11609">CVE-2020-11609</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2020-11668">CVE-2020-11668</a>

<p>Les pilotes média ov519, stv06xx et xirlink_cit ne validaient pas
correctement les descripteurs de périphérique USB. Un utilisateur
physiquement présent avec un périphérique USB spécialement construit
pouvait utiliser cela pour provoquer un déni de service (plantage) ou,
éventuellement, pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12114">CVE-2020-12114</a>

<p>Piotr Krysiuk a découvert une situation de compétition entre les
opérations umount et pivot_root dans le noyau du système de fichiers (vfs).
Un utilisateur local doté de la capacité CAP_SYS_ADMIN dans un espace de
noms d'utilisateur pourrait éventuellement utiliser cela pour provoquer un
déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12464">CVE-2020-12464</a>

<p>Kyungtae Kim a signalé une situation de compétition dans le noyau USB
qui peut avoir pour conséquence une utilisation de mémoire après libération.
Le moyen de l'exploiter n'est pas clair, mais elle pourrait avoir pour
conséquence un déni de service (plantage ou corruption de mémoire) ou une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12652">CVE-2020-12652</a>

<p>Tom Hatskevich a signalé un bogue dans les pilotes de stockage
mptfusion. Un gestionnaire ioctl récupèrait deux fois un paramètre de la
mémoire de l’utilisateur, créant une situation de compétition qui pourrait
avoir pour conséquence un verrouillage incorrect de structures de données
internes. Un utilisateur local autorisé à accéder à /dev/mptctl pourrait
utiliser cela pour provoquer un déni de service (plantage  ou corruption de
mémoire) ou pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12653">CVE-2020-12653</a>

<p>Le pilote WiFi mwifiex ne validait pas suffisamment les requêtes de
balayage, avec pour conséquence un possible dépassement de tas. Un
utilisateur local doté de la capacité CAP_NET_ADMIN pourrait utiliser cela
pour provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12654">CVE-2020-12654</a>

<p>Le pilote WiFi mwifiex ne validait pas suffisamment les paramètres WMM 
reçus d’un point d’accès (AP), avec pour conséquence un possible dépassement
de tas. Un AP malveillant pourrait utiliser cela pour provoquer un déni de
service (plantage ou corruption de mémoire) ou, éventuellement, pour exécuter
du code sur un système vulnérable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12770">CVE-2020-12770</a>

<p>Le pilote sg (SCSI générique) ne libérait pas correctement des
ressources internes dans un cas d'erreur particulier. Un utilisateur local
autorisé à accéder à un service sg pourrait éventuellement utiliser cela
pour provoquer un déni de service (épuisement de ressource).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13143">CVE-2020-13143</a>

<p>Kyungtae Kim a signalé une possible écriture de tas hors limites écrite
dans le sous-système gadget USB. Un utilisateur local autorisé à écrire
dans le système de fichiers de configuration de gadget pourrait utiliser
cela pour provoquer un déni de service (plantage ou corruption de mémoire)
ou éventuellement pour une élévation de privilèges.</p></li>

</ul>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 4.9.210-1+deb9u1. Cette version corrige aussi certains
bogues liés qui n'ont pas leur propre identifiant CVE et une régression
dans le pilote macvlan introduite dans la version intermédiaire précédente
(bogue nº 952660).</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4698.data"
# $Id: $
