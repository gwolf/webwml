#use wml::debian::translation-check translation="caa6649a706a1a437da6bc07e5727795562df8a5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité dans lynis, un outil
d’audit de sécurité. La clé de produit pourrait être obtenue par une simple
observation de la liste des processus quand un téléversement de données est
en cours de réalisation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13033">CVE-2019-13033</a>

<p>Dans Lynis 2.x de CISOfy jusqu’à la version 2.7.5, la clé de produit peut
être obtenue en regardant la liste des processus quand un téléversement de
données est en cours de réalisation. Cela peut être utilisé pour
téléverser des données à un serveur Lynis central. Bien qu’aucune donnée ne
puisse être extraite en connaissant la clé de licence, il peut être possible de téléverser
les données d’examens supplémentaires.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.6.3-1+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets lynis.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2253.data"
# $Id: $
