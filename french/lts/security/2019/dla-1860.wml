#use wml::debian::translation-check translation="cca3661334b1a0f9372a815cf550504110f2a22b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans la bibliothèque de traitement
libxslt de XSLT 1.0.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4610">CVE-2016-4610</a>

<p>Accès mémoire non valable conduisant à un déni de service de
exsltDynMapFunction. Libxslt permet à des attaquants distants de provoquer un déni
de service (corruption de mémoire) ou éventuellement d’avoir un impact non
précisé à l’aide de vecteurs inconnus.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4609">CVE-2016-4609</a>

<p>Lecture hors limites de xmlGetLineNoInternal(). Libxslt permet à des attaquants
distants de provoquer un déni de service (corruption de mémoire) ou
éventuellement d’avoir un impact non précisé à l’aide de vecteurs inconnus.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13117">CVE-2019-13117</a>

<p>Un xsl:number avec certaines chaînes de formatage pourrait conduire à une
lecture non initialisée dans xsltNumberFormatInsertNumbers. Cela pourrait
permettre à un attaquant de percevoir si un octet de la pile contient les
caractères A, a, I, i ou 0, ou tout autre caractère.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13118">CVE-2019-13118</a>

<p>Une détermination de type groupant des caractères d’une instruction
xsl:number était trop restreinte et une combinaison non valable
caractères/longueur pourrait être passée à xsltNumberFormatDecimal, conduisant
à une lecture de données de pile non initialisées.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.1.28-2+deb8u5.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libxslt.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1860.data"
# $Id: $
