#use wml::debian::translation-check translation="c4f6f852fb29b19ee8bd8acce6d635d6930a4082" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3846">CVE-2019-3846</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-10126">CVE-2019-10126</a>

<p>huangwen a signalé plusieurs dépassements de tampon dans le pilote wifi
de Marvell (mwifiex) qui pourraient être utilisés par un utilisateur
pour provoquer un déni de service ou l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5489">CVE-2019-5489</a>

<p>Daniel Gruss, Erik Kraft, Trishita Tiwari, Michael Schwarz, Ari
Trachtenberg, Jason Hennessey, Alex Ionescu et Anders Fogh ont découvert
que des utilisateurs locaux pourraient utiliser l'appel système mincore()
pour obtenir des informations sensibles d'autres processus qui accèdent au
même fichier mappé en mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11477">CVE-2019-11477</a>

<p>Jonathan Looney a signalé qu'une séquence contrefaite pour l'occasion
d'accusés de réception sélectifs («selective acknowledgement » – SACK) TCP
permet un « kernel panic » déclenchable à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11478">CVE-2019-11478</a>

<p>Jonathan Looney a signalé qu'une séquence contrefaite pour l'occasion
d'accusés de réception sélectifs (« selective acknowledgement » – SACK) TCP
fragmente la file de retransmission TCP, permettant à un attaquant de
provoquer une utilisation excessive de ressources.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11479">CVE-2019-11479</a>

<p>Jonathan Looney a signalé qu'un attaquant pourrait forcer le noyau Linux
à segmenter ses réponses en de multiples segments TCP, dont chacun contient
seulement huit octets de données, augmentant de façon drastique la bande
passante nécessaire pour distribuer la même quantité de données.</p>

<p>Cette mise à jour introduit une nouvelle valeur de sysctl pour contrôler
la valeur minimale de la longueur maximum de segment (« Maximum Segment
Size » – MSS) (net.ipv4.tcp_min_snd_mss) qui utilise par défaut la valeur
de 48, auparavant codée en dur. Il est recommandé d'augmenter cette valeur
à 512 sauf si vous savez que votre réseau nécessite une valeur plus basse.
(cette valeur s’applique uniquement pour Linux 3.16.)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11810">CVE-2019-11810</a>

<p>Le pilote megaraid_sas ne gérait pas correctement un échec d’allocation de
mémoire lors de l’initialisation, ce qui pourrait conduire à une double
libération de zone de mémoire. Cela peut avoir un impact de sécurité mais ne
peut être déclenché par un utilisateur non privilégié.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11833">CVE-2019-11833</a>

<p>L'implémentation du système de fichiers ext4 écrit des données non
initialisées de la mémoire du noyau dans de nouveaux blocs d'extension. Un
utilisateur local capable d'écrire sur un système de fichiers ext4 et puis
de lire l'image du système de fichiers, par exemple en utilisant un disque
amovible, pourrait être capable d'utiliser cela pour obtenir des
informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11884">CVE-2019-11884</a>

<p>L'implémentation de Bluetooth HIDP ne s'assurait pas que les nouveaux
noms de connexion étaient terminés par un caractère null. Un utilisateur
local doté de la capacité CAP_NET_ADMIN pourrait être capable d'utiliser
cela pour obtenir des informations sensibles de la pile du noyau.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.16.68-2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1823.data"
# $Id: $
