#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans la bibliothèque
JPEG-2000 JasPer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5203">CVE-2015-5203</a>

<p>Gustavo Grieco a découvert une vulnérabilité de dépassement d’entier qui
permet à des attaquants distants de provoquer un déni de service ou d’avoir un
autre impact non spécifié à l'aide d'un fichier d’image JPEG 2000 contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5221">CVE-2015-5221</a>

<p>Josselin Feist a trouvé une vulnérabilité de double libération de zone de
mémoire qui permet à des attaquants distants de provoquer un déni de service
(plantage d'application) en traitant un fichier image malformé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8690">CVE-2016-8690</a>

<p>Gustavo Grieco a découvert une vulnérabilité de déréférencement de pointeur
NULL qui peut provoquer un déni de service à l'aide d'un fichier image BMP
contrefait. La mise à jour inclut des correctifs pour des problèmes en rapport
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8884">CVE-2016-8884</a>
et <a href="https://security-tracker.debian.org/tracker/CVE-2016-8885">CVE-2016-8885</a>
qui complètent le correctif pour
 <a href="https://security-tracker.debian.org/tracker/CVE-2016-8690">CVE-2016-8690</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13748">CVE-2017-13748</a>

<p>jasper ne libère pas correctement la mémoire utilisée pour stocker l’image de
tuile lorsque le décodage d’image échoue. Cela pourrait conduire à un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14132">CVE-2017-14132</a>

<p>Une lecture hors limites de tampon basé sur le tas a été découverte
concernant la fonction jas_image_ishomosamp qui pourrait être déclenchée
à l'aide d'un fichier image contrefait et causer un déni de service (plantage
d'application) ou avoir un autre impact non spécifié.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 1.900.1-debian1-2.4+deb8u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jasper.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1583.data"
# $Id: $
