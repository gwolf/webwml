#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Les versions antérieures à la version 1.8-pre2 de zutils étaient sujettes
à une vulnérabilité de dépassement de tampon dans zcat provoqué par un fichier
d’entrée lorsque l’option « -v, --show-nonprinting » était utilisée (ou
indirectement activée). Cela pouvait aboutir éventuellement à un déni de
service ou l’exécution de code arbitraire. Cette attaque semble exploitable
si la victime ouvre un fichier compressé contrefait, et a été corrigée
dans 1.8-pre2.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la version 1.3-4+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zutils.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1505.data"
# $Id: $
