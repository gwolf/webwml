#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité a été découverte dans SPICE avant la version 0.14.1 où le
code utilisé pour la désérialisation (demarshalling) de messages manquait de
vérification de limites. Un serveur ou client malveillant, après authentification,
pourrait envoyer des messages contrefaits pour l'occasion à son pair. Cela
pourrait aboutir à un plantage ou, éventuellement, à d’autres impacts.</p>

<p>Le problème a été corrigé par l’amont en abandonnant avec une erreur si le
pointeur vers le début de données de message est strictement plus grand que le
pointeur vers la fin des données du message.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 0.12.5-1+deb8u6.</p>
<p>Nous vous recommandons de mettre à jour vos paquets spice.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1488.data"
# $Id: $
