#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une régression a été découverte dans la mise à jour de sécurité récente
pour 389-ds-base (389 Directory Server), publiée dans la DLA-1554-2, à cause
d’un correctif incomplet pour
<a href="https://security-tracker.debian.org/tracker/CVE-2018-14648">CVE-2018-14648</a>.
La régression provoquait un plantage du serveur lors du traitement de requêtes
avec des attributs vides.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 1.3.3.5-4+deb8u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets 389-ds-base.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1554-2.data"
# $Id: $
