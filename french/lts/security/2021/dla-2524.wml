#use wml::debian::translation-check translation="17d1d33a4ee43a02a589137e490f76fb1ff4dac3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans spice-vdagent, un agent de
client spice pour améliorer l’intégration et l’utilisation de SPICE.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15108">CVE-2017-15108</a>

<p>spice-vdagent ne protège pas correctement le répertoire <em>save</em> avant
le passage à l’interpréteur. Cela permet à un attaquant local avec accès à la
session dans laquelle l’agent s’exécute, d’injecter des commandes arbitraires
à exécuter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25650">CVE-2020-25650</a>

<p>Un défaut a été découvert dans la façon dont le démon spice-vdagentd gérait
le transfert de fichier du système hôte vers la machine virtuelle. N’importe
quel utilisateur local client non privilégié avec accès au chemin de socket
de domaine UNIX <q>/run/spice-vdagentd/spice-vdagent-sock</q> pourrait utiliser
ce défaut pour réaliser un déni de service de mémoire pour spice-vdagentd ou
même d’autres processus dans le système de la machine virtuelle. Le plus grand
risque de cette vulnérabilité concerne la disponibilité du système. Ce défaut
affecte les versions 0.20 et précédentes de spice-vdagent.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25651">CVE-2020-25651</a>

<p>Un défaut a été découvert dans le protocole de transfert de fichier de SPICE.
Des données de fichier du système hôte peuvent arriver totalement ou en partie
dans la connexion de client d’utilisateur local illégitime dans le système de
machine virtuelle. Le transfert en cours d’autres utilisateurs pourrait aussi
être interrompu, aboutissant à un déni de service. Le plus grand risque de cette
vulnérabilité est la confidentialité des données ainsi que la disponibilité du
système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25652">CVE-2020-25652</a>

<p>Un défaut a été découvert dans le démon spice-vdagentd. Ce dernier ne gérait pas
correctement les connexions de client établies à l’aide de socket de domaine
UNIX dans <q>/run/spice-vdagentd/spice-vdagent-sock</q>. N’importe quel invité
local non privilégié pourrait utiliser ce défaut pour empêcher des agents
légitime de se connecter au démon spice-vdagentd, aboutissant à un déni de
service. Le plus grand risque de cette vulnérabilité est la disponibilité du
système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25653">CVE-2020-25653</a>

<p>Une vulnérabilité de situation de compétition a été découverte dans la façon
dont le démon spice-vdagentd gérait les connexions des nouveaux clients. Ce
défaut peut permettre à un utilisateur local client de devenir un agent actif
pour spice-vdagentd, aboutissant éventuellement à un déni de service ou à une
fuite d'informations de l’hôte. Le plus grand risque de cette vulnérabilité est
la confidentialité des données ainsi que la disponibilité du système.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.17.0-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets spice-vdagent.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de spice-vdagent, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/spice-vdagent">https://security-tracker.debian.org/tracker/spice-vdagent</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2524.data"
# $Id: $
