msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-08-27 23:40+0700\n"
"Last-Translator: Izharul Haq <atoz.chevara@yahoo.com>\n"
"Language-Team: Debian Indonesia Translator <debian-l10n@debian-id.org>\n"
"Language: id_ID\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#: ../../english/distrib/search_contents-form.inc:9
#: ../../english/distrib/search_packages-form.inc:8
msgid "Keyword"
msgstr "Kata kunci"

#: ../../english/distrib/search_contents-form.inc:13
msgid "Display"
msgstr "Perlihatkan"

#: ../../english/distrib/search_contents-form.inc:16
msgid "paths ending with the keyword"
msgstr "lokasi diakhiri dengan kata kunci"

#: ../../english/distrib/search_contents-form.inc:19
msgid "packages that contain files named like this"
msgstr "paket yang mengandung nama berkas seperti ini"

#: ../../english/distrib/search_contents-form.inc:22
msgid "packages that contain files whose names contain the keyword"
msgstr ""
"paket yang mengandung berkas atau direktori yang mempunyai nama seperti kata "
"kunci"

#: ../../english/distrib/search_contents-form.inc:25
#: ../../english/distrib/search_packages-form.inc:25
msgid "Distribution"
msgstr "Distribusi"

#: ../../english/distrib/search_contents-form.inc:27
#: ../../english/distrib/search_packages-form.inc:27
msgid "experimental"
msgstr "experimental"

#: ../../english/distrib/search_contents-form.inc:28
#: ../../english/distrib/search_packages-form.inc:28
msgid "unstable"
msgstr "unstable"

#: ../../english/distrib/search_contents-form.inc:29
#: ../../english/distrib/search_packages-form.inc:29
msgid "testing"
msgstr "testing"

#: ../../english/distrib/search_contents-form.inc:30
#: ../../english/distrib/search_packages-form.inc:30
msgid "stable"
msgstr "stabil"

#: ../../english/distrib/search_contents-form.inc:31
#: ../../english/distrib/search_packages-form.inc:31
msgid "oldstable"
msgstr "oldstable"

#: ../../english/distrib/search_contents-form.inc:33
msgid "Architecture"
msgstr "Arsitektur"

#: ../../english/distrib/search_contents-form.inc:38
#: ../../english/distrib/search_packages-form.inc:32
#: ../../english/distrib/search_packages-form.inc:39
msgid "any"
msgstr "semua"

#: ../../english/distrib/search_contents-form.inc:48
#: ../../english/distrib/search_packages-form.inc:43
msgid "Search"
msgstr "Cari"

#: ../../english/distrib/search_contents-form.inc:49
#: ../../english/distrib/search_packages-form.inc:44
msgid "Reset"
msgstr "Reset"

#: ../../english/distrib/search_packages-form.inc:12
msgid "Search on"
msgstr "Cari di"

#: ../../english/distrib/search_packages-form.inc:14
msgid "Package names only"
msgstr "Hanya nama paket"

#: ../../english/distrib/search_packages-form.inc:16
msgid "Descriptions"
msgstr "Diskripsi"

#: ../../english/distrib/search_packages-form.inc:18
msgid "Source package names"
msgstr "Nama paket sumber"

#: ../../english/distrib/search_packages-form.inc:21
msgid "Only show exact matches"
msgstr "Hanya menampilkan hasil yang tepat"

#: ../../english/distrib/search_packages-form.inc:34
msgid "Section"
msgstr "Section"

#: ../../english/distrib/search_packages-form.inc:36
msgid "main"
msgstr "main"

#: ../../english/distrib/search_packages-form.inc:37
msgid "contrib"
msgstr "contrib"

#: ../../english/distrib/search_packages-form.inc:38
msgid "non-free"
msgstr "non-free"

#: ../../english/releases/arches.data:8
msgid "Alpha"
msgstr "Alpha"

#: ../../english/releases/arches.data:9
msgid "64-bit PC (amd64)"
msgstr "64-bit PC (amd64)"

#: ../../english/releases/arches.data:10
msgid "ARM"
msgstr "ARM"

#: ../../english/releases/arches.data:11
msgid "64-bit ARM (AArch64)"
msgstr "64-bit ARM (AArch64)"

#: ../../english/releases/arches.data:12
#, fuzzy
msgid "EABI ARM (armel)"
msgstr "EABI ARM"

#: ../../english/releases/arches.data:13
#, fuzzy
msgid "Hard Float ABI ARM (armhf)"
msgstr "Hard Float ABI ARM"

#: ../../english/releases/arches.data:14
msgid "HP PA-RISC"
msgstr "HP PA/RISC"

#: ../../english/releases/arches.data:15
msgid "Hurd 32-bit PC (i386)"
msgstr "Hurd (i386)"

#: ../../english/releases/arches.data:16
msgid "32-bit PC (i386)"
msgstr "32-bit PC (i386)"

#: ../../english/releases/arches.data:17
msgid "Intel Itanium IA-64"
msgstr "Intel Itanium IA-64"

#: ../../english/releases/arches.data:18
msgid "kFreeBSD 32-bit PC (i386)"
msgstr "kFreeBSD 32-bit PC (i386)"

#: ../../english/releases/arches.data:19
msgid "kFreeBSD 64-bit PC (amd64)"
msgstr "kFreeBSD 64-bit PC (amd64)"

#: ../../english/releases/arches.data:20
msgid "Motorola 680x0"
msgstr "Motorola 680x0"

#: ../../english/releases/arches.data:21
msgid "MIPS (big endian)"
msgstr "MIPS (big endian)"

#: ../../english/releases/arches.data:22
#, fuzzy
msgid "64-bit MIPS (little endian)"
msgstr "MIPS (little endian)"

#: ../../english/releases/arches.data:23
msgid "MIPS (little endian)"
msgstr "MIPS (little endian)"

#: ../../english/releases/arches.data:24
msgid "PowerPC"
msgstr "PowerPC"

#: ../../english/releases/arches.data:25
msgid "POWER Processors"
msgstr "POWER Processors"

#: ../../english/releases/arches.data:26
#, fuzzy
msgid "RISC-V 64-bit little endian (riscv64)"
msgstr "MIPS (little endian)"

#: ../../english/releases/arches.data:27
msgid "IBM S/390"
msgstr "IBM S/390"

#: ../../english/releases/arches.data:28
msgid "IBM System z"
msgstr "IBM System z"

#: ../../english/releases/arches.data:29
msgid "SPARC"
msgstr "SPARC"

#~ msgid "Intel x86"
#~ msgstr "Intel x86"

#~ msgid "AMD64"
#~ msgstr "AMD64"

#~ msgid "HP PA/RISC"
#~ msgstr "HP PA/RISC"

#~ msgid "Intel IA-64"
#~ msgstr "Intel IA-64"

#, fuzzy
#~| msgid "Intel x86"
#~ msgid "kFreeBSD (Intel x86)"
#~ msgstr "Intel x86"

#~ msgid "Hurd (i386)"
#~ msgstr "Hurd (i386)"

#~ msgid "non-US"
#~ msgstr "non-US"

#~ msgid "Search case sensitive"
#~ msgstr "Search case sensitive"

#~ msgid "Allow searching on subwords"
#~ msgstr "Perbolehkan pencarian dengan sub kata kunci"

#~ msgid "MIPS (DEC)"
#~ msgstr "MIPS (DEC)"

#~ msgid "MIPS"
#~ msgstr "MIPS"

#~ msgid "yes"
#~ msgstr "ya"

#~ msgid "no"
#~ msgstr "tidak"

#~ msgid "Case sensitive"
#~ msgstr "Perhatikan jenis huruf"

#~ msgid "all files in this package"
#~ msgstr "semua berkas di paket ini"

#~ msgid "packages that contain files or directories named like this"
#~ msgstr "paket yang mengandung berkas atau direktory seperti ini"
