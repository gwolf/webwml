<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple integer overflows have been discovered in libav 11.8 and earlier,
allowing remote attackers to cause a crash via a crafted MP3 file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
6:0.8.20-0+deb7u1.</p>

<p>We recommend that you upgrade your libav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-791.data"
# $Id: $
