<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in graphicsmagick.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14103">CVE-2017-14103</a>

    <p>The ReadJNGImage and ReadOneJNGImage functions in coders/png.c in
    GraphicsMagick 1.3.26 do not properly manage image pointers after
    certain error conditions, which allows remote attackers to conduct
    use-after-free attacks via a crafted file, related to a
    ReadMNGImage out-of-order CloseBlob call. NOTE: this vulnerability
    exists because of an incomplete fix for <a href="https://security-tracker.debian.org/tracker/CVE-2017-11403">CVE-2017-11403</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14314">CVE-2017-14314</a>

    <p>Off-by-one error in the DrawImage function in magick/render.c in
    GraphicsMagick 1.3.26 allows remote attackers to cause a denial of
    service (DrawDashPolygon heap-based buffer over-read and
    application crash) via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14504">CVE-2017-14504</a>

    <p>ReadPNMImage in coders/pnm.c in GraphicsMagick 1.3.26 does not
    ensure the correct number of colors for the XV 332 format, leading
    to a NULL Pointer Dereference.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14733">CVE-2017-14733</a>

    <p>ReadRLEImage in coders/rle.c in GraphicsMagick 1.3.26 mishandles
    RLE headers that specify too few colors, which allows remote
    attackers to cause a denial of service (heap-based buffer
    over-read and application crash) via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14994">CVE-2017-14994</a>

    <p>ReadDCMImage in coders/dcm.c in GraphicsMagick 1.3.26 allows
    remote attackers to cause a denial of service (NULL pointer
    dereference) via a crafted DICOM image, related to the ability of
    DCM_ReadNonNativeImages to yield an image list with zero frames.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14997">CVE-2017-14997</a>

    <p>GraphicsMagick 1.3.26 allows remote attackers to cause a denial of
    service (excessive memory allocation) because of an integer
    underflow in ReadPICTImage in coders/pict.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15930">CVE-2017-15930</a>

    <p>In ReadOneJNGImage in coders/png.c in GraphicsMagick 1.3.26, a
    Null Pointer Dereference occurs while transferring JPEG scanlines,
    related to a PixelPacket pointer.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, <a href="https://security-tracker.debian.org/tracker/CVE-2017-15930">CVE-2017-15930</a> has been fixed in version
1.3.16-1.1+deb7u12. The other security issues were fixed in
1.3.16-1.1+deb7u10 on 10 Oct 2017 in DLA-1130-1 but that announcement
was never sent out so this advisory also contains the notice about
those vulnerabilities.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in graphicsmagick version 1.3.16-1.1+deb7u12</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1154.data"
# $Id: $
