<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerabilitie has been discovered in the libtiff library and the
included tools, which may result in denial of service or the execution
of arbitrary code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9936">CVE-2017-9936</a>

    <p>A crafted TIFF document can lead to a memory leak resulting in a
    remote denial of service attack.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.9.6-11+deb7u7.</p>

<p>We recommend that you upgrade your tiff3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1023.data"
# $Id: $
