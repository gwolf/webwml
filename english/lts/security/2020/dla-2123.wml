<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An uninitialized pointer vulnerability was discovered in pure-ftpd, a
secure and efficient FTP server, which could result in an out-of-bounds
memory read and potential information disclosure.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.0.36-3.2+deb8u1.</p>

<p>We recommend that you upgrade your pure-ftpd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2123.data"
# $Id: $
