<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In httplib2, an attacker controlling unescaped part of uri for `httplib2.Http.request()` could change request headers and body, send additional hidden requests to same server. This vulnerability impacts software that uses httplib2 with uri constructed by string concatenation, as opposed to proper urllib building with escaping.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.9+dfsg-2+deb8u1.</p>

<p>We recommend that you upgrade your python-httplib2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2232.data"
# $Id: $
