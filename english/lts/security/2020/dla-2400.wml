<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Apache ActiveMQ, a Java message broker, uses
LocateRegistry.createRegistry() to create the JMX RMI registry and binds
the server to the <q>jmxrmi</q> entry. It is possible to connect to the
registry without authentication and call the rebind method to rebind
jmxrmi to something else. If an attacker creates another server to proxy
the original, and bound that, he effectively becomes a man in the middle
and is able to intercept the credentials when an user connects.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
5.14.3-3+deb9u1.</p>

<p>We recommend that you upgrade your activemq packages.</p>

<p>For the detailed security status of activemq please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/activemq">https://security-tracker.debian.org/tracker/activemq</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2400.data"
# $Id: $
