<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>OpenSSH was found to be vulnerable to out of order NEWKEYS messages
which could crash the daemon, resulting in a denial of service attack.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:6.0p1-4+deb7u7.</p>

<p>We recommend that you upgrade your openssh packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1257.data"
# $Id: $
