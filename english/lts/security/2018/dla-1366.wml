<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were discovered in wordpress, a web blogging
tool. The Common Vulnerabilities and Exposures project identifies the
following issues.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10100">CVE-2018-10100</a>

   <p>The redirection URL for the login page was not validated or sanitized
   if forced to use HTTPS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10102">CVE-2018-10102</a>

  <p>The version string was not escaped in the get_the_generator function,
  and could lead to cross-site scripting (XSS) in a generator tag.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.6.1+dfsg-1~deb7u21.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1366.data"
# $Id: $
