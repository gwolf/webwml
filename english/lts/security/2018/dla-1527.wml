<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Tavis Ormandy discovered multiple vulnerabilities in Ghostscript, an
interpreter for the PostScript language, which could result in denial of
service, the creation of files or the execution of arbitrary code if a
malformed Postscript file is processed (despite the dSAFER sandbox being
enabled).</p>

<p>In addition this update changes the device to txtwrite for the
ps2ascii tool to prevent an error due to the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2018-17183">CVE-2018-17183</a>.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
9.06~dfsg-2+deb8u9.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1527.data"
# $Id: $
