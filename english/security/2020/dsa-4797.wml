<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the webkit2gtk
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9948">CVE-2020-9948</a>

    <p>Brendan Draper discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9951">CVE-2020-9951</a>

    <p>Marcin Noga discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9983">CVE-2020-9983</a>

    <p>zhunki discovered that processing maliciously crafted web content
    may lead to code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13584">CVE-2020-13584</a>

    <p>Cisco discovered that processing maliciously crafted web content
    may lead to arbitrary code execution.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.30.3-1~deb10u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4797.data"
# $Id: $
