<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in the Apache HTTP server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17189">CVE-2018-17189</a>

    <p>Gal Goldshtein of F5 Networks discovered a denial of service
    vulnerability in mod_http2. By sending malformed requests, the
    http/2 stream for that request unnecessarily occupied a server
    thread cleaning up incoming data, resulting in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17199">CVE-2018-17199</a>

    <p>Diego Angulo from ImExHS discovered that mod_session_cookie does not
    respect expiry time.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0196">CVE-2019-0196</a>

    <p>Craig Young discovered that the http/2 request handling in mod_http2
    could be made to access freed memory in string comparison when
    determining the method of a request and thus process the request
    incorrectly.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0211">CVE-2019-0211</a>

    <p>Charles Fol discovered a privilege escalation from the
    less-privileged child process to the parent process running as root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0217">CVE-2019-0217</a>

    <p>A race condition in mod_auth_digest when running in a threaded
    server could allow a user with valid credentials to authenticate
    using another username, bypassing configured access control
    restrictions. The issue was discovered by Simon Kappel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0220">CVE-2019-0220</a>

    <p>Bernhard Lorenz of Alpha Strike Labs GmbH reported that URL
    normalizations were inconsistently handled. When the path component
    of a request URL contains multiple consecutive slashes ('/'),
    directives such as LocationMatch and RewriteRule must account for
    duplicates in regular expressions while other aspects of the servers
    processing will implicitly collapse them.</p></li>

</ul>

<p>For the stable distribution (stretch), these problems have been fixed in
version 2.4.25-3+deb9u7.</p>

<p>This update also contains bug fixes that were scheduled for inclusion in the
next stable point release. This includes a fix for a regression caused by a
security fix in version 2.4.25-3+deb9u6.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>For the detailed security status of apache2 please refer to its security
tracker page at: <a href="https://security-tracker.debian.org/tracker/apache2">https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4422.data"
# $Id: $
